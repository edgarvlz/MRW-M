import BackIcon from './back.png';
import Menu from './menu.png';
import CalendarIcon from './calendar.png';
import RightIcon from './right.png';
import LeftIcon from './left.png';
import CloseIcon from './close.png';
import EyeIcon from './eye.png';
import UserIcon from './user.png';
import ChatIcon from './chat.png';
import HomeIcon from './home.png';
import StadisticsIcon from './stadistics.png';
import LogoutIcon from './logout.png';
import GasClientIcon from './gas-client.png';
import HomeClientIcon from './home-client.png';
import HomeManagerIcon from './home-manager.png';
import CarManagerIcon from './car-manager.png';
import GasIcon from './gas.png';
import SendIcon from './send.png';
import SearchIcon from './search.png';
import CreateChatIcon from './create-chat.png';
import CheckIcon from './check.png';
import ReportIcon from './report.png';
import DistanceIcon from './distance.png';
import GasQuantityIcon from './gas-quantity.png';
import CameraIcon from './camera.png';
import CogIcon from './cog.png';
import CheckOutlineIcon from './check-outline.png';
import TimeIcon from './time.png';
import CarKeyIcon from './car-key.png';
import VehicleInactiveIcon from './vehicle-inactive.png';
import CouponLine from './coupon-line.png';

export {
	BackIcon,
	Menu,
	CalendarIcon,
	RightIcon,
	LeftIcon,
	CloseIcon,
	EyeIcon,
	UserIcon,
	ChatIcon,
	HomeIcon,
	StadisticsIcon,
	LogoutIcon,
	CarManagerIcon,
	HomeManagerIcon,
	HomeClientIcon,
	GasClientIcon,
	GasIcon,
	SendIcon,
	SearchIcon,
	CreateChatIcon,
	CheckIcon,
	ReportIcon,
	DistanceIcon,
	GasQuantityIcon,
	CameraIcon,
	CogIcon,
	CheckOutlineIcon,
	TimeIcon,
	CarKeyIcon,
	VehicleInactiveIcon,
	CouponLine
}