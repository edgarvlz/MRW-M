import LoginBlack from './fondo-login-black.png';
import LoginWhite from './fondo-login-white.png';
import Logo from './prevenauto-logo.png';
import LogoBlack from './logo-black.png';
import LogoMensajero from './menssajero_mrw.png';
import Wallpaper from './wallpaper_mrw.png';
import Wallpaper2 from './walpaper1.png';
import CuentaLogin from './cuenta_login.png';
import CuentaRegister from './cuenta_register.png';
import LogoMRWV from './icono_logoMRW.svg';
import Menssjerito from './menssajerito_error.png';
import Distribucion from './icon_distribucion.png';
import IconBack from './back.png';
import IconCamera from './camera.png';
import IconCamera2 from './camera2.png';
import QrCode from './qr-code.png';
import BottomPanel from './bottom-panel.png';

export {
	LoginBlack,
	LoginWhite,
	Logo,
	LogoBlack,
	LogoMensajero,
	Wallpaper,
	Wallpaper2,
	CuentaLogin,
	CuentaRegister,
	LogoMRWV,
	Menssjerito,
	Distribucion,
	IconBack,
	IconCamera,
	IconCamera2,
	QrCode,
	BottomPanel
}