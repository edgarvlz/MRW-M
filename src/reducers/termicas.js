const INITIAL_STATE = {listTermica: [],unaTermica: []};

const termicas = (state = {listTermica: [],unaTermica: []}, action) => {

	switch(action.type) {
        
        case 'LIMPIAR_TERMICA':  
            return  {
                listTermica: [],unaTermica: []
                }
            break;
        case 'SET_TERMICA':  
            return  {
                ...state,
                listTermica: [action.payload]
                }
            break;
		case 'PREPEND_TERMICA':  
            return { 
                ...state,
                listTermica: [action.payload, ...state.listTermica]
            }
            break;
        case 'APPEND_TERMICA':
            return { 
                ...state,
                listTermica: [...state.listTermica, action.payload] 
               }
            break;

        case 'REMOVE_TERMICA':
               console.log(action)
            return {
                ...state,
                listTermica: state.listTermica.filter(arrow => arrow.pickup_id !== action.payload.pickup_id)

            }
             break;

        default: 
        	return state;
        	break;
	}
}

export default termicas;
