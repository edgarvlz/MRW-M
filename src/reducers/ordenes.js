const INITIAL_STATE = {listOrdenes: [],unaOrdenes: []};

const ordenes = (state = {listOrdenes: []}, action) => {

	switch(action.type) {
        
        case 'LIMPIAR_ORDENES':  
            return  {
                listOrdenes: [],unaOrdenes: []
                }
            break;
        case 'SET_ORDENES':  
            return  {
                ...state,
                listOrdenes: [action.payload]
                }
            break;
		case 'PREPEND_ORDENES':  
            return { 
                ...state,
                listOrdenes: [action.payload, ...state.listOrdenes]
            }
            break;
        case 'APPEND_ORDENES':
            return { 
                ...state,
                listOrdenes: [...state.listOrdenes, action.payload] 
               }
            break;

        case 'REMOVE_ORDENES':
            return {
                ...state,
                listOrdenes: state.listOrdenes.filter(arrow => arrow.pickup_id !== action.payload.pickup_id)

            }
             break;
        case 'DELETE_TODO': 
             return {  // returning a copy of orignal state
              ...state, //copying the original state
              todos: state.todos.filter(todo => todo.id !== action.payload) 
                                         // returns a new filtered todos array
            }
            break;
        case 'COMPLETE_TODO': {
                const index = state.todos.findIndex(todo => todo.id !== action.payload); //finding index of the item
                const newArray = [...state.todos]; //making a new array
                newArray[index].completed = true//changing value in the new array
                return { 
                 ...state, //copying the orignal state
                 todos: newArray, //reassingning todos to new array
                }
               }
            break;

        default: 
        	return state;
        	break;
	}
}

export default ordenes;
