import { combineReducers }  from "redux";
import user                 from "./user";
import loading              from "./loading";
import pickup               from './pickup';
import ordenes              from './ordenes';
import typeScanner          from './type-scanner';
import lenguaje             from './lenguaje';
import termicas             from './termicas';
import agencias             from './agencias';
import paquetesEscaneados   from './paquetes-escaneados';
import codigoAgencia        from './codigo-agencia';
import plataforma           from './plataforma';



export default combineReducers({
  user,
  loading,
  pickup,
  ordenes,
  typeScanner,
  lenguaje,
  termicas,
  agencias,
  paquetesEscaneados,
  codigoAgencia,
  plataforma
});