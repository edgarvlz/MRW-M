const INITIAL_STATE = {listEntrante: [],listSaliente: []};

const paquetesEscaneados = (state = INITIAL_STATE , action) => {
	switch(action.type) {
		case 'SET_PAQUETES_ESCANEADOS':
			return  {
                ...state,
                listEntrante: [action.payload]
                }
            break;
		case 'APPEND_SCANER_ENTRANTE':
			return { 
				...state,
				listEntrante: [...state.listEntrante, action.payload] 
				}
			break;
		case 'REMOVE_SCANER_ENTRANTE':
            return {
                ...state,
                listEntrante: state.listEntrante.filter(arrow => arrow.listado_cupones !== action.payload)
            }
             break;
		case 'LIMPIAR_SCANER_ENTRANTE':  
            return  {
                ...state,
				listEntrante: [] 
                }
        break;
        case 'SET_PAQUETES_SALIENTE':
            return  {
                ...state,
                listSaliente: [action.payload]
                }
            break;
        case 'APPEND_SCANER_SALIENTE':
            return { 
                ...state,
                listSaliente: [...state.listSaliente, action.payload] 
                }
            break;
        case 'REMOVE_SCANER_SALIENTE':
            return {
                ...state,
                listSaliente: state.listSaliente.filter(arrow => arrow.listado_cupones !== action.payload)
            }
                break;
        case 'LIMPIAR_SCANER_SALIENTE':  
            return  {
                ...state,
                listSaliente: [] 
                }
            break;
        case 'RESET_SCANER':  
            return  {
                ...state,
				listEntrante: [],listSaliente: []
                }
            break;
        default: 
        	return state;
        	break;
	}
}

export default paquetesEscaneados;

