import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Menu } from '#/components';
import { screenOptions } from '#/navigation';

// ScreensInit

// ScreensHome
import HomeApp from '#/screens/home/home';

// //Paquetes entrante
// import PaqueteEntrante from '#/screens/paquete-entrante/paquete-entrante';
 import ScanEntrante from '#/screens/home/scan-entrante';

// //Paquetes salientes
// import PaqueteSaliente from '#/screens/home/paquete-saliente';
 import ScanSaliente from '#/screens/home/scan-saliente';


// //ScreenScan
// import ResultScan from '#/screens/home/resul-scan';
import LogoutScreen from '#/screens/logout';

const Drawer = createDrawerNavigator();
const Stack = createNativeStackNavigator();


// const PaEntranteteScreen = () => (
//     <Stack.Navigator initialRouteName="ScanEntrante" screenOptions={ screenOptions }>
//          <Stack.Screen name="ScanEntrante" component={ ScanEntrante } />
//     </Stack.Navigator>
// )

// const PaSalienteScreen = () => (
//     <Stack.Navigator initialRouteName="ScanSaliente" screenOptions={ screenOptions }>
//         <Stack.Screen name="ScanSaliente" component={ ScanSaliente } options={{
//           orientation: "portrait"
//         }} />
//     </Stack.Navigator>
// )


const HomeScreen = () => (
    <Stack.Navigator initialRouteName="HomeApp
    " screenOptions={ screenOptions } >
        {/* <Stack.Screen name="Init" component={ Init } options={{
          orientation: "portrait"
        }} /> */}
        <Stack.Screen name="HomeApp" component={ HomeApp } options={{
          orientation: "portrait"
        }} />
         <Stack.Screen name="ScanEntrante" component={ ScanEntrante } options={{
          orientation: "portrait"
        }} />
        <Stack.Screen name="ScanSaliente" component={ ScanSaliente } options={{
          orientation: "portrait"
        }} /> 
        {/*<Stack.Screen name="ResultScan" component={ ResultScan }/>*/}
    </Stack.Navigator>
)



const AdminDrawer = () => (
    
    <Drawer.Navigator initialRouteName="AdminHome" drawerContent={ (props) => <Menu {...props} />} >
        <Drawer.Screen 
            options={{headerShown: false}}
            name="AdminHome" 
            component={ HomeScreen } />
        <Drawer.Screen 
            options={{headerShown: false}}
            name="Logout" 
            component={ LogoutScreen } />
    </Drawer.Navigator>
)

export default AdminDrawer;