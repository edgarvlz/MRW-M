import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
 import AdminDrawer from './admin';

import Header from './header';
import { BackHandler } from 'react-native';
import { navigationRef } from './root-navigation';
import { Alert, Socket, SocketEvents } from '#/utils';
import { connect } from 'react-redux';
import { emit } from 'jetemit';

const Stack = createStackNavigator();

import InitScreen from '#screens/init';

// Auth
import LoginScreen from '#/screens/auth/login';
import RegisterScreen from '#/screens/auth/register';
import RegisterOnScreen from '#/screens/auth/register_on';
import RecoverScreen from '#/screens/auth/recover';
import LogoutScreen from '#/screens/logout';

const screenOptions = ({ navigation }) => ({
	header: (props) => (
		<Header
			{ ...props }
			navigation={ navigation } />
	),
	headerShown: false
});

  
class Container extends React.Component {

	constructor(props) {
	    super(props);

	    BackHandler.addEventListener("hardwareBackPress",() => {
	    	Alert.confirm('¿Desea salir de la aplicación?',() => {
	    		BackHandler.exitApp();
	    	});
	      	return true;
	    });	
	}

	componentDidMount() {
		
	}

	render() {
		return (
			<NavigationContainer ref={ navigationRef }>
				    <Stack.Navigator initialRouteName="Init" screenOptions={ screenOptions }>
				        <Stack.Screen name="Init" component={ InitScreen } />
				        <Stack.Screen name="Login" component={ LoginScreen }  options={ { headerShown: false,orientation: "portrait" } } />
				        <Stack.Screen name="Recover" component={ RecoverScreen }  options={ { headerShown: false,orientation: "portrait" } } />
				        <Stack.Screen name="Register" component={ RegisterScreen }  options={ { headerShown: false,orientation: "portrait" } } />
				        <Stack.Screen name="RegisterOn" component={ RegisterOnScreen }  options={ { headerShown: false,orientation: "portrait" } } />
				        
						<Stack.Screen name="AdminDrawer" component={ AdminDrawer } options={ { headerShown: false,orientation: "portrait" } } />
				     <Stack.Screen name="Logout" component={ LogoutScreen } />
				    </Stack.Navigator>
		    </NavigationContainer>
		)
	}
}

export default connect(state => {
	return {
		user: state.user
	}
})(Container);

export {
	Header,
	screenOptions
}