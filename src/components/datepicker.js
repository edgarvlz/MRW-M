import React from 'react';
import DatePicker from 'react-native-datepicker';
import { StyleSheet, View, Image } from 'react-native';
import { Colors } from '#/utils';
import { CalendarIcon } from '#/assets/icons';

const _DatePicker = (props) => {
	return (
		<View style={ [styles.container,props.style || {}] }>
			<DatePicker
	    		androidMode="spinner"
	    		locale="es"
	    		{...props}
	    		format={ props.format ? props.format : "DD-MM-YYYY" }
	    		onDateChange={ props.onValueChange }
	    		confirmBtnText="Aceptar"
	    		cancelBtnText="Cancelar"
	    		showIcon={ true }
	    		date={ props.value }
	    		style={ { width: '100%' } }
	    		customStyles={{
	    		  dateIcon: {
		            position: 'absolute',
		            left: 0,
		            top: 10,
		            marginLeft: 5,
		            tintColor: Colors.blue,
		            width: 20,
		            height: 20
		          },
		          dateInput: {
		            borderWidth: 0,
		            backgroundColor: Colors.gray,
		            marginLeft: 33,
		            borderTopRightRadius: 3,
		            borderBottomRightRadius: 3,
					alignItems: 'flex-start',
					paddingHorizontal: 5,
					height: 40
		          }
		        }}
		        iconSource={ CalendarIcon } />
		</View>		
	)
}

const styles = StyleSheet.create({
	container: {
		borderRadius: 5,
		height: 40,
		flexDirection: 'row',
		backgroundColor: Colors.gray,
		overflow: 'hidden',
	}
});

export default _DatePicker;
