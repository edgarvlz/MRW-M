import React from 'react';
import { StyleSheet, View, Image, TouchableOpacity, Text, ScrollView } from 'react-native';
import { Colors, Constants, SocketEvents } from '#/utils';
import { useSelector, useDispatch } from 'react-redux';
import { ChatService } from '#/services';
import { CommonActions } from '@react-navigation/native';
import { on } from 'jetemit';
import { LogoBlack,LogoMensajero } from '#/assets/img';
import { HomeIcon, LogoutIcon, ChatIcon, StadisticsIcon } from '#/assets/icons';

const ItemsAdmin = [
	{ title: 'Inicio', page: 'AdminHome', icon: HomeIcon },
	// { title: 'Mis productos', page: 'LectorQR', icon: StadisticsIcon },
	{ title: 'Cerrar sesión', page: 'Logout', icon: LogoutIcon },
];


const replace = (navigation,page) => {
	navigation.dispatch(
	  CommonActions.reset({
	    index: 0,
	    routes: [
	      { name: page }
	    ],
	  })
	)
}

const Menu = (props) => {
	const user = useSelector(state => state.user);


	return (
		<View style={ styles.container }>
			<ScrollView>
				<View style={ styles.containerWhite }>
					<Image source={ LogoMensajero } style={ styles.logo } />
				</View>				
				<View style={ styles.containerName }>
				{
						user && (
					<Text style={ styles.name }>{ user.nombres } { user.apellidos } </Text>
					)					
				}
				</View>
				<View style={ styles.containerMenu }>

					{
						ItemsAdmin.map((item,index) => (
							<TouchableOpacity key={ index } onPress={ () => replace(props.navigation,item.page) }>
								<View style={ styles.item }>
									<Image source={ item.icon } style={ styles.icon } />
									<Text style={ styles.text }>{ item.title }</Text>	
								</View>
							</TouchableOpacity>
						))
					}
						
				</View>
			</ScrollView>
		</View>
	)
}
	
const styles = StyleSheet.create({
	container: {
		backgroundColor: Colors.gray,
		flex: 1
	},
	containerWhite: {
		backgroundColor: Colors.white,
		height: 200
	},
	logo: {
		width: '90%',
		alignSelf: 'center',
		resizeMode: 'contain',
		height: 200
	},
	item: {
		borderBottomWidth: 1,
		borderBottomColor: Colors.black,
		flexDirection: 'row',
		padding: 10,
		alignItems: 'center'
	},
	containerName: {
		padding: 10,
		backgroundColor: Colors.primary,
	},
	name: {
		color: Colors.white,
		fontSize: 14,
		fontFamily: 'Montserrat-Bold'
	},
	icon: {
		width: 25,
		height: 25,
		resizeMode: 'contain',
		marginLeft: 10
	},
	text: {
		marginLeft: 15,
		fontFamily: 'Montserrat-Regular'
	},
	badge: {
		backgroundColor: Colors.blue,
		width: 20,
		height: 20,
		borderRadius: 10,
		justifyContent: 'center',
		alignItems: 'center',
		marginLeft: 10,
		overflow: 'hidden'
	},
	textBadge: {
		color: Colors.white,
		fontSize: 12
	}
});

export default Menu;