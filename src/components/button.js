import React from 'react';
import { TouchableOpacity, View, Text, StyleSheet, Platform } from 'react-native';
import Icon from './icon';
import { Colors } from '#/utils';

const Button = (props) => (
	<TouchableOpacity onPress={ props.onPress }>
		<View style={ [styles.container,props.style || {}] }>
			<View style={ { flex: .1 }}>
				{ props.icon && !props.iconRight && (
					<Icon name={ props.icon } 
						size={ props.iconSize }
						color={ props.iconColor }
					/>
				) }
			</View>
			<View style={ { flex: .8, alignItems: 'center' }}>
				{ props.title && <Text style={ props.textStyle } numberOfLines={ props.nowrap ? 1 : null }>{ props.title }</Text> }
			</View>
			<View style={ { flex: .1 }}>
				{ props.icon && props.iconRight && (
					<Icon name={ props.icon } 
						size={ props.iconSize }
						color={ props.iconColor }
					/>
				) }
			</View>			
		</View>
	</TouchableOpacity>
)

const styles = StyleSheet.create({
	container: {
		alignSelf: 'flex-start',
		minWidth: 200,
		backgroundColor: Colors.primary,
		padding: 7,
		borderRadius: 5,
		height: 38,
		paddingTop: Platform.OS == 'android' ? 5 : undefined,
		fontFamily:'Montserrat-ExtraBold'
	}
});

export default Button;