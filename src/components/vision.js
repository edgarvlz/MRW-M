import React from 'react';
import { RNCamera } from 'react-native-camera';
import { Icon } from '#/components';
import { StyleSheet, View, Dimensions } from 'react-native';
import { Colors } from '#/utils';
import { TouchableOpacity } from 'react-native-gesture-handler';

class Vision extends React.Component {

	state = {
      takingPic: false,
    }

	takePicture = async () => {
	    if (this.camera && !this.state.takingPic) {

	      let options = {
	        quality: 0.85,
	        fixOrientation: true,
	        forceUpOrientation: true,
	      };

	      this.setState({
	      	takingPic: true
	      });

	      try {
	        const res = await this.camera.takePictureAsync(options);
	        const uri = Platform.OS === "android" ? res.uri : res.uri.replace("file://", "");
	    	let name = res.fileName;
	        if (typeof fileName === "undefined") {
	            const getFilename = uri.split("/");
	            name = getFilename[getFilename.length - 1];
	        }
	        this.props.onClose({
	        	uri,
	            name,
	            type: 'image/jpeg'
	        });
	      } catch (err) {
	        console.log(err);
	      } finally {
	        this.setState({
	        	takingPic: false
	        });
	      }
	    }
	}
	
	render() {
		return (
			<React.Fragment>
				<View style={ styles.buttonClose }>
			      <TouchableOpacity onPress={ () => this.props.onClose() }>
			      	<Icon name="md-close" size={ 40 } color={ Colors.white }></Icon>
			      </TouchableOpacity>
		        </View>
				<RNCamera 
			      ref={ref => {
			        this.camera = ref;
			      }}
			      captureAudio={ false }
			      style={ styles.camera }
			      type={ RNCamera.Constants.Type.back } />
			      <View style={ styles.button }>
				      <TouchableOpacity onPress={ this.takePicture }>
				      	<Icon name="md-camera" size={ 80 } color={ Colors.white }></Icon>
				      </TouchableOpacity>
			      </View>
			</React.Fragment>
		)
	}
}

const styles = StyleSheet.create({
	camera: {
		width: Dimensions.get('window').width,
		height: Dimensions.get('window').height,
	},
	button: {
		alignItems: 'center',
		position: 'absolute',
		bottom: 40,
		width: '100%',
		zIndex: 999
	},
	buttonClose: {
		position: 'absolute',
		top: 20,
		right: 10,
		zIndex: 999
	}
});

export default Vision;