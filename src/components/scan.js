import React from 'react';
import {
    CameraDeviceFormat,
    CameraRuntimeError,
    FrameProcessorPerformanceSuggestion,
    PhotoFile,
    sortFormats,
    useCameraDevices,
    useFrameProcessor,
    VideoFile,
    Camera
  } from 'react-native-vision-camera';
  


 const Scaner = (props) => {
   const devices = useCameraDevices()
   const device = devices.back
   if (device == null) {
     return <ActivityIndicator size={20} color={'red'} />;
   }
   return (
     <Camera
       style={{flex: 1}}
       device={device}
       isActive={true}
       frameProcessorFps={'auto'}
     />
   );
 }
export default Scaner;