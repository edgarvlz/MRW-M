import React from 'react';
import RNPickerSelect from 'react-native-picker-select';
import { View, StyleSheet, Text, TouchableWithoutFeedback, Image } from 'react-native';
import { Colors } from '#/utils';

const Select = (props) => {
	const items = props.items || [{ value: '1', label: 'Prueba' }, { value: '2', label: 'Prueba2' }];
	const placeholder = { value: '', label: props.placeholder || 'Seleccione' };

	return (
		<View style={ [styles.container,props.style || {}] }>
			{
				props.icon && !props.iconRight && (
					<View style={ { flex: .1 } }>
						<TouchableWithoutFeedback onPress={ props.onPressIcon }>
							<Image source={ props.icon } style={ [styles.icon,props.iconStyle || {}] } />
						</TouchableWithoutFeedback>
					</View>
				)
			}
			<View style={ { flex: props.icon ? .9 : 1 } }>
				<RNPickerSelect
				    {...props}
				    useNativeAndroidPickerStyle={ false }
				    style={{
					  	inputAndroid: styles.input,
					  	inputIOS: styles.input
					}}
				    doneText="Aceptar"
				    placeholder={ placeholder }
				    items={ items }
				/>
			</View>
			{
				props.icon && props.iconRight && (
					<View style={ { flex: .1, alignItems: 'center' } }>
						<TouchableWithoutFeedback onPress={ props.onPressIcon }>
							<Image source={ props.icon } style={ [styles.icon,props.iconStyle || {}] } />
						</TouchableWithoutFeedback>
					</View>		
				)
			}		
		</View>
	)	
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: Colors.gray,
		marginBottom: 10,
		paddingLeft: 3,
		paddingRight: 3,
		borderRadius: 5,
		flexDirection: 'row',
		alignItems: 'center'
	},
	input: {
		height: 38
	},
	icon: {
		width: 30,
		height: 30,
		resizeMode: 'contain',
		marginRight: 20
	},
	input: {
		height: 38,
		fontSize: 14,
		color: 'black'
	}
});

export default Select;