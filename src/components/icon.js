import React from 'react';
import Icon from 'react-native-vector-icons/dist/Ionicons';

const _Icon = (props) => (
	<Icon name={ props.name } size={ props.size } color={ props.color } />
)

export default _Icon;