import React from 'react';
import { TextInput, StyleSheet, View, TouchableWithoutFeedback, Image, Text } from 'react-native';
import { Colors } from '#/utils';

const Input = (props) => (
	<View style={ [styles.container, props.style || {}] }>
		{
			props.icon && !props.iconRight && (
				<View style={ { flex: .1 } }>
					<TouchableWithoutFeedback onPress={ props.onPressIcon }>
						<Image source={ props.icon } style={ [styles.icon,props.iconStyle || {}] } />
					</TouchableWithoutFeedback>
				</View>
			)
		}
						

		{
			props.numeric && (
				<View style={ { flex: .9 } }>
				<TextInput 
					{...props} 
					style={ [styles.input, props.inputStyle || {}] }
					keyboardType = 'numeric'

				/>
			</View>
				)
		}
		{
			!props.numeric &&(
				<View style={ { flex: .9 } }>
				<TextInput 
					{...props} 
					style={ [styles.input, props.inputStyle || {}] }
					
	
				/>
			</View>
			)
		}
	
		{
			props.icon && props.iconRight && (
				<View style={ { flex: .1, alignItems: 'center' } }>
					<TouchableWithoutFeedback onPress={ props.onPressIcon }>
						<Image tintColor={props.tintColor} source={ props.icon } style={ [styles.icon, props.iconStyle || {}] } />
					</TouchableWithoutFeedback>
				</View>		
			)
		}
		{
			props.textRight && (
				<View style={ { flex: .1, alignItems: 'center' } }>
					<Text style={{ color: Colors.gray2 }}>{ props.textRight }</Text>
				</View>		
			)
		}
	</View>
)

const styles = StyleSheet.create({
	container: {
		backgroundColor: Colors.white,
		marginBottom: 10,
		paddingLeft: 3,
		paddingRight: 3,
		borderRadius: 5,
		flexDirection: 'row',
		alignItems: 'center'
	},
	input: {
		height: 38,
		fontFamily:'Montserrat-Regular'
	},
	icon: {
		width: 30,
		height: 30,
		resizeMode: 'contain',
		marginRight: 20
	}
});

export default Input;