import React from 'react';
import { SafeAreaView, ScrollView, View,RefreshControl } from 'react-native';
import { Colors } from '#/utils';

const ScreenContainer = (props) => (

	<SafeAreaView style={ { flex: 1, backgroundColor: props.backgroundColor || Colors.gray } }>
		<ScrollView contentContainerStyle={{ flexGrow: 1 }}>
			{ props.children }
		</ScrollView>
	</SafeAreaView>
)

export default ScreenContainer;