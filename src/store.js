import { createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import rootReducer from './reducers';
import Config from 'react-native-config';

import {APP_NAME} from "@env"
console.log(APP_NAME)
const persistConfig = {
  key: APP_NAME,
  storage: AsyncStorage,
}
const persistedReducer = persistReducer(persistConfig, rootReducer)
const store = createStore(persistedReducer)
const persistor = persistStore(store)

export { store, persistor }