export default {
	LEVELS: {
		ADMIN: 1,
		MANAGER: 2,
		CLIENT: 3
	},
	CURRENCIES: {
		DOLAR: '$'
	},
	ORDER_STATUS: {
		PENDING: 'pendiente',
		READY: 'atendida',
		CANCELED: 'anulada'
	}
}