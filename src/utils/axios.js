import axios from "axios";
import {APP_BASE_API} from "@env"
console.log(APP_BASE_API)
import Config from 'react-native-config';
// var url = 'http://192.168.85.47:8005/api/'
// var url = 'http://167.172.141.140/api/'

let _axios = axios.create({
  baseURL: APP_BASE_API
  //baseURL:'http://192.168.85.47:8005/api/'
});
console.log(APP_BASE_API)
const createFormData = (data) => {
  var formdata = new FormData();
  for (var key in data) {
    if (Array.isArray(data[key])) {
      for (var _key in data[key]) {
        if (Array.isArray(data[key][_key])) {
          for (var i in data[key][_key]) {
            formdata.append(key + '[' + _key + '][' + i + ']', data[key][_key][i]);
          }
        }
        else {
          formdata.append(key + '[' + _key + ']', data[key][_key]);
        }
      }      
    }
    else {
      formdata.append(key, data[key]);
    }    
  }
  return formdata;
}

_axios.all = axios.all;
_axios.spread = axios.spread;
_axios.upload = (url, data) => {
  return _axios.post(url, createFormData(data), {
    headers: {
      "Content-Type": "multipart/form-data"
    }
  });
};
export default _axios;
