import Lenguaje from './prueba-idioma';
import Colors from './colors';
import Styles from './styles';
import Fonts from './fonts';
import Api from './api';
import Alert from './alert';
import Globals from './globals';
import Constants from './constants';
import Socket from './socket';
import SocketEvents from './socket-events';
import Validate from './fbasic';
import ApiMarketplace from './api-marketplace';
import DataFake from './data-fake';

export {
	Api,
	Colors,
	Styles,
	Fonts,
	Alert,
	Globals,
	Constants,
	Socket,
	SocketEvents,
	Validate,
	ApiMarketplace,
	DataFake,
	Lenguaje,

}