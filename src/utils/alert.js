import { Alert } from 'react-native';
import prompt from 'react-native-prompt-android';

class _Alert {
	
	showAlert(message,buttons = null,options = null) {
		Alert.alert('Alerta',message,buttons,options);
	}

	showError(message = null) {
		Alert.alert('Alerta',message || 'Por favor, revise su conexión');
	}

	confirm = (message,callback) => {
		Alert.alert('Confirmar',message,[
		    {
		    	text: 'Cancelar'
		    },
		    {
		    	text: 'Aceptar',
		    	onPress: () => {
		    		callback();
		    	}
		    }
		]);
	}

	notification = (message,callback) => {
		Alert.alert('Confirmar',message,[
		  
		    {
		    	text: 'Aceptar',
		    	onPress: () => {
		    		callback();
		    	}
		    }
		]);
	}

	input = (message,callback) => {
		prompt(
	      '',
	      message,
	      [
	        {
	          text: "Cancelar"
	        },
	        {
	          text: "Aceptar",
	          onPress: response => {
	          	callback(response);
	          }
	        },
	      ],
	      {
	      	type: 'plain-text'
	      }
	    )
	}

	confirm2 = (message,callback) => {
		Alert.alert('Confirmar',message,[
		    {
		    	text: 'Cancelar',
		    	onPress: () => {
		    		callback("cancelar");
		    	}
		    },
		    {
		    	text: 'continuar',
		    	onPress: () => {
		    		callback("aceptar");
		    	}
		    }
		]);
	}
}

export default new _Alert();