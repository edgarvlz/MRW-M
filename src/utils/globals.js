import { store } from '#/store';
import Toast from 'react-native-root-toast';
import Config from 'react-native-config';
import Constants from './constants';


var urlBaseSotorage  ="http://104.248.233.24/Prevenauto/api/public/storage/"
class Globals {
	BASE_STORAGE = urlBaseSotorage;
	
	showLoading() {
		store.dispatch({
			type: 'SET_LOADING'
		});
	}

	quitLoading() {
		store.dispatch({
			type: 'QUIT_LOADING'
		});
	}

	formatMiles = (n, decimals = true, currency = Constants.CURRENCIES.DOLAR, conversion = 1) => {
		var c = isNaN(c = Math.abs(c)) ? (decimals ? 2 : 0) : c,
			d = d == undefined ? "," : d,
			t = t == undefined ? "." : t,
			s = n < 0 ? "-" : "",
			i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
			j = (j = i.length) > 3 ? j % 3 : 0;

		const amount = ' ' + s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");

		if (currency == Constants.CURRENCIES.DOLAR)
			return Currency.DOLAR + amount;
		else if (currency == Constants.CURRENCIES.BOLIVAR)
			return Currency.BOLIVAR + amount;
		else if (currency == 'Km')
			return amount + ' ' + currency;
		else return amount;
	}

	sendToast(message) {
	    Toast.show(message,{
	        duration: Toast.durations.SHORT,
	        position: Toast.positions.BOTTOM
	    });
	}

	validateDouble(value) {
		return /^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/.test(value);
	}

	validateInteger(value) {
		return /^\d+$/.test(value);
	}

	clone = (data) => {
		return JSON.parse(JSON.stringify(data));
	}

	clean = (_string) => {
	   var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,.";
	   for (var i = 0; i < specialChars.length; i++) {
	       _string= _string.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
	   }
	   _string = _string.toLowerCase();
	   _string = _string.replace(/ /g,"_");
	   _string = _string.replace(/á/gi,"a");
	   _string = _string.replace(/é/gi,"e");
	   _string = _string.replace(/í/gi,"i");
	   _string = _string.replace(/ó/gi,"o");
	   _string = _string.replace(/ú/gi,"u");
	   _string = _string.replace(/ñ/gi,"n");
	   return _string;
  	}
	
}

export default new Globals();