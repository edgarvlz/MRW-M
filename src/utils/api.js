import Error from './error';
import axios from './axios';
import Globals from './globals';

class Api {
	
	createResponse = (uri,params = {}) => {
		return new Promise((resolve,reject) => {
			if (!params.withoutLoading) {
				Globals.showLoading();
			}
			axios[params.hasFile ? 'upload' : 'post'](uri,params)
				.then((res) => {
					if (!params.withoutLoading) {
						Globals.quitLoading();
					}
					setTimeout(() => {
						resolve(res.data);
					},100);						
				})
				.catch((err) => {
					if (!params.withoutLoading) {
						Globals.quitLoading();
					}
					setTimeout(() => {
						if (!params.withoutLoading && !params.withoutError) {
							Error.default(err);
						}						
						reject(err);
					},100);
				});
		});
	}
}

export default new Api();