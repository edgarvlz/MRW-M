import Error from './error';
import axiosMarketplace from './axios-marketplace';
import Globals from './globals';

class ApiMarketplace {
	
	createResponse = (uri,params = {}) => {
		
		return new Promise((resolve,reject) => {
			if (!params.withoutLoading) {
				Globals.showLoading();
			}
			axiosMarketplace['get'](uri)
				.then((res) => {
					//alert(res.data)
					if (!params.withoutLoading) {
						Globals.quitLoading();
					}
					setTimeout(() => {
						resolve(res.data);
					},100);						
				})
				.catch((err) => {
				
					if (!params.withoutLoading) {
						Globals.quitLoading();
					}
					setTimeout(() => {
						if (!params.withoutLoading && !params.withoutError) {
							Error.default(err);
						}						
						reject(err);
					},100);
				});
		});
	}
}

export default new ApiMarketplace();