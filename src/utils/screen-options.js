import React from 'react';
import Header from '#/navigation/header';

const screenOptions = ({ navigation }) => ({
	header: (props) => (
		<Header
			{ ...props }
			navigation={ navigation } />
	),
	headerShown: false
});

export default {
	screenOptions
}