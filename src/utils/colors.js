export default {
	primary:    '#25294a',
	secondary: '#e30613',
	gray: '#f2f2f2',
	white: '#fff',
	yellow: '#e8c221',
	cyan: '#c6ecff',
	blue: '#00a1ff',
	danger: '#e53935',
	gray2: '#757575',
	sky: '#c6ecff',
	green: '#29e074',
	gray3: '#6d6d75',
	gray4: '#e9e9e9',
	black: '#141414',
	cancel: '#eb3446',
	accept: '#44c77b',
	darkorange: '#ff5500',
	blackTrans: '#1414145d',
	gray4: '#9E9E9E',
	graphics: {
		color1: '#97dbff',
		color2: '#67c8ff',
		color3: '#33b5ff',
		color4: '#00a3ff',
		color5: '#007abe',
		color6: '#005281'
	}
}