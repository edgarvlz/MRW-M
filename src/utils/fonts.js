export default {
	REGULAR: 'Poppins-Regular',
	MEDIUM: 'Poppins-SemiBold',
	BOLD:   'Poppins-Bold'

}
