import Alert from './alert';
import { navigationRef } from '#/navigation/root-navigation';
import { CommonActions } from '@react-navigation/native';

class Error {
	
	default = (err) => {
		if (err.response && err.response.status === 422) {
			Alert.showAlert(err.response.data.error);
         	return;
		}
		else if(err.code=="ERR_NETWORK"){

				Alert.showError();
				var navigation = navigationRef?.current;
				// navigation.navigate('Scan');
		}
		else {
			console.log(err);
		}
	}
}

export default new Error();