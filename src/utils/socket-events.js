export default {
	CHAT: {
		MESSAGE: 'chat/message'
	},
	REPORTS: {
		CRASH: 'reports/crash',
		DISTANCE: 'reports/distance'
	}
}