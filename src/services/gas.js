import { Api } from '#/utils';

class GasService {
	
	static get = async (params = {}) => Api.createResponse('gas/get',params);
	static create = async (params = {}) => Api.createResponse('gas/create',params);

	static manager = {
		get: async (params = {}) => Api.createResponse('manager/gas/get',params),
	}

	static admin = {
		get: async (params = {}) => Api.createResponse('admin/gas/get',params),
	}

}

export default GasService;