import { Api } from '#/utils';

class ChatService {
	
	static get = async (params = {}) => Api.createResponse('chat/get',params);
	static messages = async (params = {}) => Api.createResponse('chat/messages',params);
	static viewed = async (params = {}) => Api.createResponse('chat/viewed',params);
	static send = async (params = {}) => Api.createResponse('chat/send',params);

	static manager = {
		get: async (params = {}) => Api.createResponse('manager/chat/get',params),
		messages: async (params = {}) => Api.createResponse('manager/chat/messages',params),
		viewed: async (params = {}) => Api.createResponse('manager/chat/viewed',params),
		send: async (params = {}) => Api.createResponse('manager/chat/send',params),
		drivers: async (params = {}) => Api.createResponse('manager/chat/drivers',params),
		create: async (params = {}) => Api.createResponse('manager/chat/create',params)
	}

}

export default ChatService;