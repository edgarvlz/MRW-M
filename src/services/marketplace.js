import { ApiMarketplace } from '#/utils';

class Marketplace {
	
	static home = {
		publicaciones_nuevas: async (params = {}) => ApiMarketplace.createResponse('publicaciones-nuevas',params),
	}


}

export default Marketplace;