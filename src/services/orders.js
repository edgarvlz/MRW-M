import { Api } from '#/utils';

class OrderService {

	static manager = {
		get: async (params = {}) => Api.createResponse('manager/orders/get',params),
		view: async (params = {}) => Api.createResponse('manager/orders/view',params),
	}

	static admin = {
		get: async (params = {}) => Api.createResponse('admin/orders/get',params),
		view: async (params = {}) => Api.createResponse('admin/orders/view',params),
	}

}

export default OrderService;