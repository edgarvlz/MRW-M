import { Api } from '#/utils';

class VehicleService {
	
	static get = async (params = {}) => Api.createResponse('vehicle/get',params);
	static getCrash = async (params = {}) => Api.createResponse('vehicle/crash',params);
	static createCrash = async (params = {}) => Api.createResponse('vehicle/crash/create',params);

	static manager = {
		get: async (params = {}) => Api.createResponse('manager/vehicles/get',params),
		getCrash: async (params = {}) => Api.createResponse('manager/vehicles/crash',params),
		getCount: async (params = {}) => Api.createResponse('manager/vehicles/count',params),
		view: async (params = {}) => Api.createResponse('manager/vehicles/view',params),
		deleteDriver: async (params = {}) => Api.createResponse('manager/vehicles/delete-driver',params),
		getUsers: async (params = {}) => Api.createResponse('manager/vehicles/users',params),
		assign: async (params = {}) => Api.createResponse('manager/vehicles/assign',params)
	}

	static admin = {
		get: async (params = {}) => Api.createResponse('admin/vehicles/get',params),
		getCrash: async (params = {}) => Api.createResponse('admin/vehicles/crash',params),
		getCount: async (params = {}) => Api.createResponse('admin/vehicles/count',params),
		view: async (params = {}) => Api.createResponse('admin/vehicles/view',params),
		deleteDriver: async (params = {}) => Api.createResponse('admin/vehicles/delete-driver',params),
		getUsers: async (params = {}) => Api.createResponse('admin/vehicles/users',params),
		assign: async (params = {}) => Api.createResponse('admin/vehicles/assign',params)
	}
}

export default VehicleService;