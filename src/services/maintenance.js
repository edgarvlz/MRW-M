import { Api } from '#/utils';

class MaintenanceService {

	static manager = {
		get: async (params = {}) => Api.createResponse('manager/maintenance/get',params),
	}

	static admin = {
		get: async (params = {}) => Api.createResponse('admin/maintenance/get',params),
	}

}

export default MaintenanceService;