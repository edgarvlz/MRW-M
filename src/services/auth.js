import { Api } from '#/utils';


class AuthService {
	static login = async (params = {}) => Api.createResponse('login/app',params);
	static register = async (params = {}) => Api.createResponse('auth/register',params);
	static recover = async (params = {}) => Api.createResponse('auth/recover',params);
}

export default AuthService;