import { Api } from '#/utils';

class StadisticsService {

	static manager = {
		get: async (params = {}) => Api.createResponse('manager/stadistics/get',params),
		getCrash: async (params = {}) => Api.createResponse('manager/stadistics/crash',params),
		getDistance: async (params = {}) => Api.createResponse('manager/stadistics/distance',params),
	}

	static admin = {
		get: async (params = {}) => Api.createResponse('admin/stadistics/get',params),
		getCrash: async (params = {}) => Api.createResponse('admin/stadistics/crash',params),
		getDistance: async (params = {}) => Api.createResponse('admin/stadistics/distance',params),
	}

}

export default StadisticsService;