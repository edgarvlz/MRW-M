import { Api } from '#/utils';


class OrdenPickup {
	static consultar = async (params = {}) => Api.createResponse('consultar/pickup',params);
	static status = async (params = {}) => Api.createResponse('cambio/status',params);
}

export default OrdenPickup;