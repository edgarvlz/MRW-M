import { Api } from '#/utils';


class SincroService {
	static sincronizarEntrante = async (params = {}) => Api.createResponse('sincronizar/entrante',params);
	static sincronizarSaliente = async (params = {}) => Api.createResponse('sincronizar/saliente',params);
}

export default SincroService;