import AuthService from './auth';
import OrderService from './orders';
import OrdenPickup from './pickup';
import SincroService from './sincro';

export {
	AuthService,
	OrderService,
	OrdenPickup,
	SincroService
}