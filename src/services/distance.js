import { Api } from '#/utils';

class DistanceService {
	
	static get = async (params = {}) => Api.createResponse('distance/get',params);
	static create = async (params = {}) => Api.createResponse('distance/create',params);

	static manager = {
		get: async (params = {}) => Api.createResponse('manager/distance/get',params),
	}

	static admin = {
		get: async (params = {}) => Api.createResponse('admin/distance/get',params),
	}

}

export default DistanceService;