import React from 'react';
import { connect } from 'react-redux';
import { Constants, Globals } from '#/utils';

class Init extends React.Component {

	componentDidMount() {
		// this.props.dispatch({
		// 	type: 'SET_LENGUAJE',
		// 	payload: {idioma:'1'}
		// });

		this.props.navigation.setParams({
			hideHeader: true
		});
		
		this.load();
	}

	load = async () => {
		if (!this.props.user) {
			// this.props.dispatch({
			// 	type: 'SET_CODIGO_AGENCIA',
			// 	payload: null
			// });
			this.props.navigation.replace('Login');
		}
		else {
				// this.props.dispatch({
				// 	type: 'SET_CODIGO_AGENCIA',
				// 	payload: null
				// });

				this.props.navigation.replace('AdminDrawer');
			
		}		
	}
	
	render() {
		return null;
	}
}

export default connect(state => {
	return {
		user: state.user
	}
})(Init);