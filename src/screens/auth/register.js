import React from 'react';
import { ScreenContainer, Button, Input,Select } from '#/components';
import { Colors, Styles, Fonts, Constants ,Alert} from '#/utils';
import { StyleSheet, View, TouchableOpacity, Text, ImageBackground, Image,Dimensions} from 'react-native';
import { AuthService } from '#/services';
import { connect } from 'react-redux';
import { navigationRef } from '#/navigation/root-navigation';
import { CommonActions } from '@react-navigation/native';
import { UserIcon, EyeIcon } from '#/assets/icons';
import { LoginWhite, Logo,  LogoMensajero, Wallpaper, Wallpaper2, CuentaLogin, CuentaRegister } from '#/assets/img';


const isPortrait = () => {
    const dim = Dimensions.get('screen');
    return dim.height >= dim.width;
};

/**
 * Returns true of the screen is in landscape mode
 */
const isLandscape = () => {
    const dim = Dimensions.get('screen');
    return dim.width >= dim.height;
};

class RegisterScreen extends React.Component {

	state = {
		form: {
			nombres: '',
			apellidos: '',
			cliente: '',
			dni: '',
			email: '',
			password: '',
			check1:false

		},
		checked:false ,
		setErrorNombre: '',
		setErrorApellido: '',
		setErrorTdocumento: '',
		setErrorDni: '',
		setErrorEmail: '',
		setErrorClave: '',
		
		array: [
			{
			  name: 'Venezolano',
			  value: 'V',
			},
			{
			  name: 'Extranjero',
			  value: 'E',
			},
			{
			  name: 'Pasaporte',
			  value: 'P',
			},
		  ],
		visible_pass: false
	}
	
    
	componentDidMount() {
		this.props.navigation.setParams({
			hideHeader: false
		});
	
	}

	change = (value,target) => {
		//Asignar valores a las variables
		this.setState({
			form: {
				...this.state.form,
				[target]: value
			}
		});
		//Vaciar mensajes de error
		this.setState({
			
			setErrorNombre: '',
			setErrorApellido: '',
			setErrorTdocumento: '',
			setErrorDni: '',
			setErrorEmail: '',
			setErrorClave: '',
			
		});

	}

	recover = () => {
		this.props.navigation.navigate('Recover');
	}
	login = () => {
		this.props.navigation.navigate('Login');
	}

	submit = async () => {
		 const navigation = navigationRef?.current;

		 const res = await AuthService.register({
		 	...this.state.form
		 });

		 if(res.registro==true){
			navigation.dispatch(
				CommonActions.reset({
					index: 0,
					routes: [{
						name: 'RegisterOn'
					}]
				})
			)
		 }
	
		 
		
		
	
	}

	formValidate = () => {

		this.props.navigation.navigate('Recover');

	}
	
	render() {
		return (
			<ScreenContainer backgroundColor={ Colors.white }>
				
				<ImageBackground resizeMode='cover'

				    // source={isPortrait() ? Wallpaper2 : Wallpaper }
				     source={ Wallpaper2  }

					style={ styles.backgroundStyles }
				>

					<View style={{marginTop: 20,}}>
						<Text style={styles.labelBienvenido}>Crear perfil</Text>
					</View>


					<Image
						source={ LogoMensajero }
						style={ styles.imageStyles }
					/>
					
					<View style={ styles.containerButtons1 }>
						<View style={{ flex: .5 }}>
							<TouchableOpacity  onPress={ this.login }    style={ styles.optionsLogin }>
								<Image source={ CuentaLogin } style={ styles.imageStyles1 }/>
								<Text style={styles.tabInactive}>
								Inicio de sesión
								</Text>
							</TouchableOpacity>
						</View>
						<View style={{ flex: .5 }}>
							<View  style={ styles.optionsLogin }>
								<Image source={ CuentaRegister } style={ styles.imageStyles1 }/>
								<Text style={styles.tabActive}>
								Registrarse
								</Text>
							</View>
						</View>
					</View>
					
					
					<View style={ styles.form }>
						<Text style={styles.label}>Nombres</Text>
						<Input
							inputStyle={ styles.inputStyle }
							placeholderTextColor={Colors.black}
							onChangeText={ e => this.change(e,'nombres') }

							//onKeyPress="return valida(event,this,4,100)"

							placeholder=""
							iconRight={ true }
							iconStyle={{ width: 20, height: 20 }}
							icon={ UserIcon }
							style={styles.input}
						/>
						{this.state.setErrorNombre != '' &&
							<Text style={styles.label3}>{this.state.setErrorNombre}</Text>
						}

						<Text style={styles.label}>Apellidos</Text>
						<Input
							inputStyle={ styles.inputStyle }
							placeholderTextColor={Colors.black}
							onChangeText={ e => this.change(e,'apellidos') }
							placeholder=""
							iconRight={ true }
							iconStyle={{ width: 20, height: 20 }}
							icon={ UserIcon }
							style={styles.input}
						/>
						{this.state.setErrorApellido != '' &&
							<Text style={styles.label3}>{this.state.setErrorApellido}</Text>
						}
						<Text style={styles.label}>Tipo documento</Text>
						<Select 
							onValueChange={ value => {
								this.setState({
									form: {
										...this.state.form,
										cliente: value
									}
								});
							} }

							value={ this.state.form.cliente }
							style={ styles.input }
							iconRight={ true }
							iconStyle={{ width: 20, height: 20 }}
							icon={ UserIcon }
							items={ this.state.array.map((item) => {
								return {
									value: item.value,
									label: item.name
								}
							}) }
						/>
					
						{this.state.setErrorTdocumento != '' &&
							<Text style={styles.label3}>{this.state.setErrorTdocumento}</Text>
						}
						<Text style={styles.label}>Número de documento</Text>
						<Input
							inputStyle={ styles.inputStyle }
							placeholderTextColor={Colors.black}
							onChangeText={ e => this.change(e,'dni') }
							placeholder=""
							iconRight={ true }
							iconStyle={{ width: 20, height: 20 }}
							icon={ UserIcon }
							style={styles.input}
							numeric = 'numeric'

						/>
						{this.state.setErrorDni != '' &&
							<Text style={styles.label3}>{this.state.setErrorDni}</Text>
						}
						<Text style={styles.label}>Correo electrónico</Text>
						<Input
							inputStyle={ styles.inputStyle }
							placeholderTextColor={Colors.black}
							onChangeText={ e => this.change(e,'email') }
							iconRight={ true }
							iconStyle={{ width: 20, height: 20 }}
							icon={ UserIcon }
							style={styles.input}
						/>
					
						{this.state.setErrorEmail != '' &&
							<Text style={styles.label3}>{this.state.setErrorEmail}</Text>
						}

						<Text style={styles.label}>Contraseña</Text>
						<Input
							secureTextEntry={ !this.state.visible_pass }
							placeholder=""
							placeholderTextColor={Colors.black}
							iconRight={ true }
							icon={ EyeIcon }
							iconStyle={{ width: 20, height: 20 }}
							onPressIcon={ () => {
								this.setState({
									visible_pass: !this.state.visible_pass
								});
							} }
							style={styles.input}
							onChangeText={ e => this.change(e,'password') } />
						{this.state.setErrorClave != '' &&
							<Text style={styles.label3}>{this.state.setErrorClave}</Text>
						}

						
					
						<View style={ styles.buttonContainer }>
							<Button 
								onPress={ this.submit }
								textStyle={ styles.buttonText }
								style={ styles.buttonPrimary }

								title="Registrarme"
							/>
						</View>					
					</View>
				

				<View style={ styles.links }>
					<TouchableOpacity onPress={ this.recover }>
						<Text style={[ styles.forgotPasswordText ] }>
							¿Olvidaste tu contraseña?
						</Text>
					</TouchableOpacity>
				</View>		
				</ImageBackground>

				
			</ScreenContainer>
		)
	}
}

const styles = StyleSheet.create({
	icon: {
		width: 22.5,
		height: 22.5,
		resizeMode: 'contain'
	},
	form: {
		width: '100%',
		borderRadius: 10,
		padding:30
	},
	buttonText: {
		textAlign: 'center',
		fontFamily: 'Montserrat-ExtraBold',
		color: Colors.white
	},
	buttonContainer: {
		marginVertical: 20,
		alignItems: 'center',
	},
	forgotPasswordText: {
		textAlign: 'center',
		color: Colors.black,
		fontFamily: 'Montserrat-Light',

	},
	register: {
		color: Colors.cyan,
		fontFamily: 'Montserrat-ExtraBold',
		marginLeft: 10,
	},
	bg: {
		width: '100%',
		paddingBottom: 20
	},
	imageBg: {
		resizeMode: 'stretch'
	},
	logo: {
		width: 200,
		height: 150,
		alignSelf: 'center',
		marginTop: 40,
		marginBottom: 30,
		resizeMode: 'contain'
	},
	links: {
		marginBottom: 30
	},
	header: {
		padding: 20
	},
	buttonRegisterContainer: {
		flex: 1,
		justifyContent: 'center',
		flexDirection: 'row',
	},
	label: {
		color: Colors.bla,
		marginBottom: 5,
		fontSize: 13,
		fontFamily: 'Montserrat-Bold'
	},
	label1: {
		color: Colors.bla,
		marginBottom: 5,
		fontSize: 25,
		fontFamily: 'Montserrat-Bold'
	},
	label3: {
		color: Colors.danger,
		marginBottom: 5,
		fontSize: 11,

	},
	lineContainer: {
		alignItems: 'center',
	},
	line: {
		width: '70%',
		borderTopWidth: 1,
		borderColor: Colors.gray2,
	},
	backgroundStyles: {
		alignItems: 'center',
		height:'100%'
	},

	labelDistribucion: {
		color: Colors.secondary,
		marginBottom: 5,
		fontSize: 30,
		fontWeight: 'bold',
		textAlign:'center',
	},
	labelBienvenido: {
		color: Colors.black,
		marginTop: 5,
		fontSize: 20,
		textAlign:'center',
		fontFamily:'Montserrat-Regular'

	},
	imageStyles: {
		width: '70%',
		height: 100,
		maxHeight:70,

		resizeMode: 'contain',
		marginTop: 0,
	},
	imageStyles1: {

		width: '45%',
		resizeMode: 'contain',
	},
	containerButtons1:{
		width: '100%',
	
		flexDirection: 'row',
		alignItems: 'center'
	},
	
	optionsLogin:{
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
	},
	
	input: {
		backgroundColor: Colors.white,
		borderColor: Colors.black,
		borderWidth:1
	},
	tabActive:{
		fontFamily:'Montserrat-Bold'
	},
	tabInactive:{
		fontFamily:'Montserrat-Regular'
	},
	buttonPrimary:{
		backgroundColor: Colors.primary,
		borderRadius: 20,
		fontFamily:'Montserrat-Bold'
	},
	buttonSecondary:{
		backgroundColor: Colors.secondary,
		borderRadius: 20,
		fontFamily:'Montserrat-Bold'

	},
});

export default connect((state) => {
	return {
	}
})(RegisterScreen);