import React from 'react';
import { ScreenContainer, Button, Input } from '#/components';
import { Colors, Styles, Fonts, Constants } from '#/utils';
import { StyleSheet, View, TouchableOpacity, Text, ImageBackground, Image,Dimensions  } from 'react-native';
import { AuthService } from '#/services';
import { connect } from 'react-redux';
import { navigationRef } from '#/navigation/root-navigation';
import { CommonActions } from '@react-navigation/native';
import { UserIcon, EyeIcon } from '#/assets/icons';
import { LoginWhite, Logo,  LogoMensajero, Wallpaper, Wallpaper2, CuentaLogin, CuentaRegister } from '#/assets/img';

const isPortrait = () => {
    const dim = Dimensions.get('screen');
    return dim.height >= dim.width;
};

/**
 * Returns true of the screen is in landscape mode
 */
const isLandscape = () => {
    const dim = Dimensions.get('screen');
    return dim.width >= dim.height;
};

class LoginScreen extends React.Component {

	state = {
		form: {
			email: '',
			password: ''
		},
		visible_pass: false
	}

	componentDidMount() {
		this.props.dispatch({
			type: 'SET_LENGUAJE',
			payload: {idioma:'1'}
		});
		
		this.props.navigation.setParams({
			hideHeader: false
		});
		console.log(this.props.paquetesEscaneados)
		this.props.dispatch({
			type: 'LIMPIAR_SCANER_ENTRANTE',
			payload: null
		});	
	}

	change = (value,target) => {
		this.setState({
			form: {
				...this.state.form,
				[target]: value
			}
		});
	}

	recover = () => {
		this.props.navigation.navigate('Recover');
	}
	register = () => {
		this.props.navigation.navigate('Register');
	}

	submit = async () => {
		const navigation = navigationRef?.current;

		const res = await AuthService.login({
			...this.state.form
		});
		await this.props.dispatch({
			type: 'SET_USER',
			payload: res.user
		});
		if (res.user.usuario_perfil_id == 34) {
			navigation.dispatch(
				CommonActions.reset({
					index: 0,
					routes: [{
						name: 'AdminDrawer'
					}]
				})
			)
		}		
	}
	
	render() {
		return (
			<ScreenContainer backgroundColor={ Colors.white }>
				
				<ImageBackground resizeMode='cover'

				    // source={isPortrait() ? Wallpaper2 : Wallpaper }
				     source={ Wallpaper2  }

					style={ styles.backgroundStyles }
				>

					<View style={{marginTop: 20,}}>
						<Text style={styles.labelBienvenido}>Bienvenido a</Text>
								
					</View>


					<Image
						source={ LogoMensajero }
						style={ styles.imageStyles }
					/>
					{/* <View >
						<Text style={styles.labelDistribucion}>Ecommerce</Text>
								
					</View> */}
					<View style={ styles.containerButtons1 }>
						<View style={{ flex: 5 }}>
							<View  style={ styles.optionsLogin }>
								<Image source={ CuentaLogin } style={ styles.imageStyles1 }/>
								<Text style={styles.tabActive}>
								Inicio de sesión
								</Text>
							</View>
						</View>
						{/* <View style={{ flex: .5 }}>
							<TouchableOpacity onPress={ this.register }  style={ styles.optionsLogin }>
								<Image source={ CuentaRegister } style={ styles.imageStyles1 }/>
								<Text style={styles.tabInactive}>
								Registrarse
								</Text>
							</TouchableOpacity> 
						</View> */}
					</View>
					
					
					<View style={ styles.form }>
						<Text style={styles.label}>Usuario</Text>
						<Input
							inputStyle={ styles.inputStyle }
							placeholderTextColor={Colors.black}
							onChangeText={ e => this.change(e,'email') }
							placeholder="Usuario o correo electrónico"
							iconRight={ true }
							iconStyle={{ width: 20, height: 20 }}
							icon={ UserIcon }

							style={styles.input}
						/>
						<Text style={styles.label}>Contraseña</Text>
						<Input
							secureTextEntry={ !this.state.visible_pass }
							placeholder="Contraseña"
							placeholderTextColor={Colors.black}
							iconRight={ true }
							icon={ EyeIcon }
							iconStyle={{ width: 20, height: 20 }}
							onPressIcon={ () => {
								this.setState({
									visible_pass: !this.state.visible_pass
								});
							} }
							style={styles.input}

							onChangeText={ e => this.change(e,'password') } />
							<View style={ styles.links }>
							<TouchableOpacity onPress={ this.recover }>
								<Text style={[ styles.forgotPasswordText ] }>
									¿Olvidaste tu contraseña?
								</Text>
							</TouchableOpacity>
						</View>					
					</View>
				

					<View style={ styles.buttonContainer }>
						<Button 
							onPress={ this.submit }
							textStyle={ styles.buttonText }
							style={ styles.buttonSecondary }
							title="Entrar"

						/>
					</View>		
				
				</ImageBackground>

				
			</ScreenContainer>
		)
	}
}

const styles = StyleSheet.create({
	icon: {
		width: 19,
		height: 19,
		resizeMode: 'contain'
	},
	form: {
		width: '100%',
		borderRadius: 10,
		padding:30
	},
	buttonText: {
		textAlign: 'center',
		fontFamily: 'Montserrat-Regular',
		color: Colors.white,
		fontSize: 18,

	},
	buttonContainer: {
		marginVertical: 20,
		alignItems: 'center',
	},
	forgotPasswordText: {
		textAlign: 'center',
		color: Colors.black,
		fontFamily: 'Montserrat-Light',

	},
	register: {
		color: Colors.cyan,
		fontFamily: 'Montserrat-ExtraBold',
		marginLeft: 10,
	},
	bg: {
		width: '100%',
		paddingBottom: 20
	},
	imageBg: {
		resizeMode: 'stretch'
	},
	logo: {
		width: 200,
		height: 150,
		alignSelf: 'center',
		marginTop: 40,
		marginBottom: 30,
		resizeMode: 'contain'
	},
	links: {
		marginTop: 20
	},
	header: {
		padding: 20
	},
	buttonRegisterContainer: {
		flex: 1,
		justifyContent: 'center',
		flexDirection: 'row',
	},
	label: {
		color: Colors.bla,
		marginBottom: 5,
		fontSize: 13,
		fontFamily: 'Montserrat-Bold'

	},
	label1: {
		color: Colors.bla,
		marginBottom: 5,
		fontSize: 25,
		fontFamily: 'Montserrat-Bold'
	},
	
	lineContainer: {
		alignItems: 'center',
	},
	line: {
		width: '70%',
		borderTopWidth: 1,
		borderColor: Colors.gray2,
	},
	backgroundStyles: {
		alignItems: 'center',
		height:'100%'
	},

	labelDistribucion: {
		color: Colors.secondary,
		marginBottom: 5,
		fontSize: 30,
		fontWeight: 'bold',
		textAlign:'center',
	},
	labelBienvenido: {
		color: Colors.black,
		marginTop: 5,
		fontSize: 20,
		textAlign:'center',
		fontFamily:'Montserrat-Regular'
	},
	imageStyles: {
		width: '70%',
		height: 100,
		maxHeight:70,
		resizeMode: 'contain',
		marginTop: 0,
	},
	imageStyles1: {
		width: '45%',
		resizeMode: 'contain',
	},
	containerButtons1:{
		width: '100%',
	
		flexDirection: 'row',
		alignItems: 'center'
	},
	
	optionsLogin:{
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
	},
	
	input: {
		backgroundColor: Colors.white,
		borderColor: Colors.black,
		borderWidth:1
	},
	buttonPrimary:{
		backgroundColor: Colors.primary,
		borderRadius: 20,
		fontFamily:'Montserrat-Bold'
	},
	buttonSecondary:{
		backgroundColor: Colors.secondary,
		borderRadius: 20,
		fontFamily:'Montserrat-Bold'

	},
	tabActive:{
		fontFamily:'Montserrat-Bold'
	},
	tabInactive:{
		fontFamily:'Montserrat-Regular'
	},
	
});

export default connect((state) => {
	return {
		user: state.user,
        paquetesEscaneados: state.paquetesEscaneados,

	}
})(LoginScreen);