import React from 'react';
import { ScreenContainer, Button, Input } from '#/components';
import { Colors, Styles, Fonts, Constants } from '#/utils';
import { StyleSheet, View, TouchableOpacity, Text, ImageBackground, Image,Dimensions  } from 'react-native';
import { AuthService } from '#/services';
import { connect } from 'react-redux';
import { navigationRef } from '#/navigation/root-navigation';
import { CommonActions } from '@react-navigation/native';
import { UserIcon, EyeIcon } from '#/assets/icons';
import { LoginWhite, Logo,  LogoMensajero, Wallpaper, Wallpaper2, CuentaLogin, CuentaRegister ,Menssjerito} from '#/assets/img';

const isPortrait = () => {
    const dim = Dimensions.get('screen');
    return dim.height >= dim.width;
};

/**
 * Returns true of the screen is in landscape mode
 */
const isLandscape = () => {
    const dim = Dimensions.get('screen');
    return dim.width >= dim.height;
};

class RegisterOnScreen extends React.Component {

	state = {
		form: {
			email: '',
			password: ''
		},
		visible_pass: false
	}

	componentDidMount() {
		this.props.navigation.setParams({
			hideHeader: false
		});
	
	}

	change = (value,target) => {
		this.setState({
			form: {
				...this.state.form,
				[target]: value
			}
		});
	}


	login = () => {
		this.props.navigation.navigate('Login');
	}

	submit = async () => {
		const navigation = navigationRef?.current;

		const res = await AuthService.login({
			...this.state.form
		});
		await this.props.dispatch({
			type: 'SET_USER',
			payload: res.user
		});
		

		navigation.dispatch(
			CommonActions.reset({
				index: 0,
				routes: [{
					name: 'AdminDrawer'
				}]
			})
		)
		
		
		
	}
	
	render() {
		return (
			<ScreenContainer backgroundColor={ Colors.white }>
				
				<ImageBackground resizeMode='cover'

				    // source={isPortrait() ? Wallpaper2 : Wallpaper }
				     source={ Wallpaper2  }

					style={ styles.backgroundStyles }
				>



					<Image
						source={ Menssjerito }
						style={ styles.imageStyles }
					/>
					
					<View style={{marginTop: 20,}}>
						<Text style={styles.labelCorreo}>Confirma tu correo electrónico</Text>
								
					</View>
					<View style={{marginTop: 20,marginLeft:20, marginRight:20}}>
						<Text style={styles.label}>Antes de continuar, por favor, confirma tu correo electrónico con el enlace que te hemos enviado. Si no has recibido el email,</Text><TouchableOpacity onPress={ this.recover }>
						<Text style={[ styles.labelLink ] }>
						pulsa aquí para que te enviemos otro.
						</Text>
					</TouchableOpacity>
								
					</View>
					
					<View style={ styles.buttonContainer }>
						<Button 
							onPress={ this.login }
							textStyle={ styles.buttonText }
							title="Iniciar sesión"
						/>
					</View>		
			
					{/* <View >
						<Text style={styles.labelDistribucion}>Ecommerce</Text>
								
					</View> */}
					
					
					
					

				
				</ImageBackground>

				
			</ScreenContainer>
		)
	}
}

const styles = StyleSheet.create({
	icon: {
		width: 22.5,
		height: 22.5,
		resizeMode: 'contain'
	},
	form: {
		width: '100%',
		borderRadius: 10,
		padding:30
	},
	buttonText: {
		textAlign: 'center',
		fontFamily: 'Montserrat-ExtraBold',
		color: Colors.white
	},
	buttonContainer: {
		marginVertical: 20,
		alignItems: 'center',
		paddingTop:20
	},
	forgotPasswordText: {
		textAlign: 'center',
		color: Colors.black,
	},
	register: {
		color: Colors.cyan,
		fontFamily: 'Montserrat-ExtraBold',
		marginLeft: 10,
	},
	bg: {
		width: '100%',
		paddingBottom: 20
	},
	imageBg: {
		resizeMode: 'stretch'
	},
	logo: {
		width: 200,
		height: 150,
		alignSelf: 'center',
		marginTop: 40,
		marginBottom: 30,
		resizeMode: 'contain'
	},
	links: {
		marginBottom: 30
	},
	header: {
		padding: 20
	},
	buttonRegisterContainer: {
		flex: 1,
		justifyContent: 'center',
		flexDirection: 'row',
	},
	label: {
		color: Colors.bla,
		marginBottom: 5,
		fontSize: 15,
		textAlign:'center'
	},
	labelLink: {
		color: Colors.secondary,
		fontSize: 15,
		textAlign:'center'
	},
	label1: {
		color: Colors.bla,
		marginBottom: 5,
		fontSize: 25,
		fontFamily: 'Montserrat-Bold'
	},
	
	lineContainer: {
		alignItems: 'center',
	},
	line: {
		width: '70%',
		borderTopWidth: 1,
		borderColor: Colors.gray2,
	},
	backgroundStyles: {
		alignItems: 'center',
		height:'100%'
	},

	labelDistribucion: {
		color: Colors.secondary,
		marginBottom: 5,
		fontSize: 30,
		fontWeight: 'bold',
		textAlign:'center',
	},
	labelCorreo: {
		color: Colors.secondary,
		marginTop: 5,
		fontSize: 20,
		fontWeight:'bold',
		textAlign:'center',
	},
	imageStyles: {
		width: '70%',
		height: 100,
		maxHeight:70,
		resizeMode: 'contain',
		marginTop: 100,
	},
	imageStyles1: {
		width: '45%',
		resizeMode: 'contain',
	},
	containerButtons1:{
		width: '100%',
	
		flexDirection: 'row',
		alignItems: 'center'
	},
	
	optionsLogin:{
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
	},
	
	input: {
		backgroundColor: Colors.white,
		borderColor: Colors.black,
		borderWidth:1
	},
});

export default connect((state) => {
	return {
	}
})(RegisterOnScreen);