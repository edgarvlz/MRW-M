import React from 'react';
import { ScreenContainer, Button, Input } from '#/components';
import { Text, StyleSheet, View, ImageBackground, Image } from 'react-native';
import { Styles, Colors, Fonts, Alert } from '#/utils';
import { AuthService } from '#/services';
import { LoginWhite,Menssjerito } from '#/assets/img';

class Recover extends React.Component {

	state = {
		form: {
			email: ''
		}
	}

	componentDidMount() {
		this.props.navigation.setParams({
			hideHeader: false
		});
	}

	change = (value,target) => {
		this.setState({
			form: {
				...this.state.form,
				[target]: value
			}
		});
	}

	submit = async () => {
		await AuthService.recover({
			...this.state.form
		});
		Alert.showAlert("Se le ha enviado un correo electrónico para la recuperación de contraseña");
		this.props.navigation.goBack();
	}
	
	render() {
		return (
			<ScreenContainer backgroundColor={ Colors.white }>
				<ImageBackground
				
				>
					<View style={{ padding: 10 }}>
						

					<Image
						source={ Menssjerito }
						style={ styles.imageStyles }
					/>
						<View style={ styles.form }>
							<Text style={ styles.label }>Ingrese su correo electrónico y recibirá un código de acceso</Text>
							<Input
								inputStyle={ styles.inputStyle }
								onChangeText={ e => this.change(e,'email') }
								placeholder="Correo Electrónico" 
								style={styles.input}/>		
						</View>
						<View style={ styles.buttonContainer }>
							<Button
								onPress={ this.submit }
								textStyle={ styles.buttonText }
								title="Enviar"
								
								style={ styles.buttonPrimary } />
						</View>
					</View>		
				</ImageBackground>		
			</ScreenContainer>
		)
	}
}

const styles = StyleSheet.create({
	title: {
		color: Colors.yellow,
		textAlign: 'center',
		fontSize: 16,
		marginVertical: 20
	},
	form: {
		width: '100%',
		marginVertical: 10,
		borderRadius: 10,
		padding: 20,
		paddingBottom: 30
	},
	buttonText: {
		textAlign: 'center',
		fontFamily: 'Montserrat-ExtraBold',
		color: Colors.white
	},
	buttonContainer: {
		alignItems: 'center'
	},
	bg: {
		width: '100%',
		height: '100%',
		paddingBottom: 20
	},
	imageBg: {
		height: '70%'
	},
	label: {
		textAlign: 'center',
		marginBottom: 20,
		color: Colors.bla,
		fontFamily: 'Montserrat-Regular',
	},
	icon: {
		width: 90,
		height: 90,
		alignSelf: 'center',
		marginTop: 30
	},
	backgroundStyles: {
		alignItems: 'center',
	},
	input: {
		backgroundColor: Colors.white,
		borderColor: Colors.black,
		borderWidth:1
	},
	imageStyles: {
		height: 100,
		maxHeight:100,
		resizeMode: 'contain',
	},
	
	tabActive:{
		fontFamily:'Montserrat-Bold'
	},
	buttonPrimary:{
		backgroundColor: Colors.primary,
		borderRadius: 20,
		fontFamily:'Montserrat-Bold'
	},
});

export default Recover;