import React from 'react';
import { connect } from 'react-redux';
import { Platform } from 'react-native';

class Logout extends React.Component {

	componentDidMount() {
		this.props.navigation.setParams({
			hideHeader: true
		});

		this.load();
	}

	load = async () => {	
		await this.props.dispatch({
			type: 'LIMPIAR_SCANER_ENTRANTE',
			payload: null
		});	
		await this.props.dispatch({
			type: 'SET_USER',
			payload: null
		});
		await this.props.dispatch({
			type: 'SET_PICKUP',
			payload: null
		});
		this.props.navigation.replace('Login');
	}
	
	render() {
		return null;
	}
}

export default connect(state => {
	return {
		user: state.user,
	}
})(Logout);