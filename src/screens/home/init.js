import React from 'react';
import { connect } from 'react-redux';
import { Constants, Globals } from '#/utils';

class Init extends React.Component {

	componentDidMount() {
		// this.props.dispatch({
		// 	type: 'SET_LENGUAJE',
		// 	payload: {idioma:'1'}
		// });

		this.props.navigation.setParams({
			hideHeader: true
		});
		
		this.load();
	}

	load = async () => {
		

		 if (!this.props.codigoAgencia) {
		 	 this.props.dispatch({
		 	 	type: 'SET_CODIGO_AGENCIA',
		 	 	payload: null
		 	 });
		 	this.props.navigation.replace('HomeApp');
		 }
		 else {
		 	if(!this.props.paquetesEscaneados.listEntrante[0]){
		 		alert("existe codigo de agencia, pero no listado")
		 		this.props.dispatch({
		 			type: 'SET_CODIGO_AGENCIA',
		 			payload: null
		 		});
		 		this.props.navigation.replace('HomeApp');

		 	}else{
		 		alert("existe codigo de agencia y listado")
		 		this.props.navigation.navigate('PaEntrante', { screen: 'ScanEntrante' })
		 	}
		 }		
	}
	
	render() {
		return null;
	}
}

export default connect(state => {
	return {
		user: state.user,
		codigoAgencia: state.codigoAgencia,
		paquetesEscaneados: state.paquetesEscaneados,

	}
})(Init);