import React, { Fragment,Component  } from 'react';
import { StyleSheet, View,  Text, Image,Dimensions,TouchableOpacity,  ImageBackground, Vibration} from 'react-native';
import { connect } from 'react-redux';
import { SincroService } from '#/services';
import { IconBack,IconCamera,IconCamera2,QrCode,BottomPanel } from '#/assets/img';
// import { Scaner } from '#/components';
import { RNCamera } from 'react-native-camera';
import { Alert } from '#/utils';
import moment from 'moment';

const isPortrait = () => {
    const dim = Dimensions.get('screen');
    return dim.height >= dim.width;
};

var deviceWidth  =  Dimensions.get('window').width ;
var deviceHeight =  Dimensions.get('window').height ;

//  console.log("w",deviceWidth,"h",deviceHeight,isPortrait() ?  "Ve" : "Ho",)

class Scan extends Component {


	constructor(props) {

	    super(props);
		this.state = {
            scan: false,
            ScanResult: false,
            result: null,
            orientation:     isPortrait() ? true : false,
            deviceWidth :    Dimensions.get('window').width,
            deviceHeight :   Dimensions.get('window').heightn,
            scanPaquetes :   this.props.paquetesEscaneados.listSaliente,
            user :           this.props.user,
            camera: {
                type: RNCamera.Constants.Type.back,
                flashMode: RNCamera.Constants.FlashMode.auto,
              }

        };
        this.barcodeCodes = [];

        //   this.props.dispatch({
        //       type: 'RESET_SCANER',
        //       payload: null
        //   });
          console.log(this.props.paquetesEscaneados)
        Dimensions.addEventListener('change', () => {
            this.setState({
                orientation: isPortrait() ?  true : false,
            });
        });
       
		

	}
    
	onBarCodeRead(scanResult) {
        console.warn(scanResult.type);
        
        
        Vibration.vibrate()

        if(scanResult.type =='QR_CODE'){
            var dataSplit = scanResult.data.split(';')
            var dataQR =  {

                    agencia_origen:         dataSplit[0],
                    agencia_destino:        dataSplit[1],
                    fecha:                  moment().format('YYYY/MM/DD H:mm:00'),
                    peso:                   dataSplit[3],
                    tipo_cupon:             dataSplit[4],
                    primer_cupon:           dataSplit[5],
                    listado_cupones:        dataSplit[6],

                    tipo_cobro:             dataSplit[7],
                    corporativo_o_precio:   dataSplit[8],

                    nombre_emisor:          dataSplit[9],
                    dni_emisor:             dataSplit[10],
                    tlf_emisor:             dataSplit[11],

                    nombre_recep:           dataSplit[12],
                    tlf_recep:              dataSplit[13],
                    dni_recep:              dataSplit[14],
                    
                    traking:                dataSplit[15],



                    // envio_id:           4,
                    // agencia_id:         dataSplit[1],
                    // nro_envio:          dataSplit[2],
                    // fecha:              dataSplit[3],
                    // nombre_emisor:      dataSplit[4],
                    // tlf_emisor:         dataSplit[5],
                    // email_emisor:       dataSplit[6],
                    // tipo_dni_emisor:    dataSplit[7],
                    // dni_emisor:         dataSplit[8],
                    
                    // nombre_recep:       dataSplit[9],
                    // tlf_recep:          dataSplit[10],
                    // email_recep:        dataSplit[11],
                    // tipo_dni_recep:     dataSplit[12],
                    // dni_recep:          dataSplit[13],

                    // direccion:          dataSplit[6],
                    // poblacion_dest:     dataSplit[7],
                    // agencia_recep_id:   dataSplit[8],
                    // descripcion:        dataSplit[9],

                    // observacion:        dataSplit[10],
                    // ent_agencia:        dataSplit[11],
                    // peso:               dataSplit[12],
                    // medidas:            dataSplit[13],
                    // num_guia_alterno:   dataSplit[14],
                    // total_resguardo:    dataSplit[15],
                    // calculo_resguardo:  dataSplit[16],
                    // costo_embalaje:     dataSplit[17],
                    // cant_cupones:       dataSplit[18],
                    // costo_cupones:      dataSplit[19],
                    // costo_domicilio:    dataSplit[20],
                    // codigo:             dataSplit[21],
                    // sincronizado:       dataSplit[22],
                    // created_at:         dataSplit[23],
                    // updated_at:         dataSplit[24],
                    
                    // nombre_recep:       dataSplit[27],
                    // tlf_recep:          dataSplit[28],
                    // estado_id:          dataSplit[29],
                    // cod:                dataSplit[30],
                    // email_recep:        dataSplit[32],
                    // envios_estatus_id:  dataSplit[33],
                    // tipo_dni_recep:     dataSplit[34],
                    // resguardo:          dataSplit[35],
                    // dni_recep:          dataSplit[36],
                    // cupon_id:           dataSplit[37],
                    // total_pagar:        dataSplit[38],
                    // manifiesto_id:      dataSplit[39],
                    // iva:                dataSplit[40],
                    // metodo_pago_id:     dataSplit[41],
                    // usuario_id:         dataSplit[42],
                    // tipo:               dataSplit[43],
                    // valor_cupon:        dataSplit[44],
                    // seriales:           dataSplit[45]
            };

            var prueba = this.state.scanPaquetes.find(element => element.listado_cupones == dataQR.listado_cupones);
            if(prueba != undefined){
                alert('El paquete ya se encuentra escaneado')
            }else{
                this.setState({
                    ...this.state.scanPaquetes,
                    scanPaquetes: [...this.state.scanPaquetes, dataQR] 
    
                },() => {
                    this.props.dispatch({
                        type: 'APPEND_SCANER_SALIENTE',
                        payload: dataQR
                    });
                });

            }

        }
        this.setState({
            result: scanResult,
            scan: false,
            ScanResult: false
        })
        
    
      }
	activeQR = () => {
        this.setState({ scan: true })
    }
    scanAgain = () => {
        this.setState({ scan: false, ScanResult: false })
    }
	
	componentDidMount() {
	}


    componentWillUnmount() {
    }
      
    sincronizar = async  () => {
            var paquetes = this.state.scanPaquetes
            console.log('separador')

            // console.log(this.state.user)
            for (const paquete of paquetes) {
                 const res = await SincroService.sincronizarSaliente({
                     paqueteSal: paquete,
                     user: this.state.user,
                     estatus_id: 4,
                 });
                 console.log(res)
                if(res.menssage=='existe arriba'){
                    
                    var  listOrdenes = this.state.scanPaquetes.filter(arrow => arrow.listado_cupones !== paquete.listado_cupones)
                    this.setState({
                        scanPaquetes: listOrdenes        
                    })

                    await this.props.dispatch({
                        type: 'REMOVE_SCANER_SALIENTE',
                        payload: paquete.listado_cupones
                    });
                }else if(res.menssage=='registrado'){

                    var  listOrdenes = this.state.scanPaquetes.filter(arrow => arrow.listado_cupones !== paquete.listado_cupones)
                    this.setState({
                        scanPaquetes: listOrdenes        
                    })

                    await this.props.dispatch({
                        type: 'REMOVE_SCANER_SALIENTE',
                        payload: paquete.listado_cupones
                    });
                }
            }

        }
    exitScan = () => {
       
        if (this.state.scanPaquetes.length != 0) {
            Alert.confirm2('Tienes paquetes por sincronizar, ¿Deseas salir?', (r) => {
                if (r == "cancelar") {
                
                } else if (r == "aceptar") {
                    this.props.dispatch({
                        type: 'LIMPIAR_SCANER_SALIENTE',
                        payload: null
                    });
                    this.props.navigation.goBack()
                }
            });
        }else{
            this.props.navigation.goBack()
        }
    }
	render() {
        const { scan, ScanResult, result,orientation,deviceWidth,deviceHeight,scanPaquetes } = this.state

        return (
            <View style={styles.scrollViewStyle}>
                <Fragment>
                    <View style={styles.header}>
                        <TouchableOpacity 
						    onPress={this.exitScan}>
						<Image source={IconBack} style={{height: 36, width: 36}}></Image>
                        </TouchableOpacity>
                        <Text style={styles.textTitle}></Text>
                    </View>

                    {/*****  Antes de escanear *****/}
                    
                    {/* Vertical */}
                    {!scan && !ScanResult && 
                      
                        <View style={ styles.cardView.vertical  } >
                            
                            <Image source={IconCamera} style={{height: 25, width: 25}}></Image>
                            <Text numberOfLines={8} style={styles.descText}>Mueva su cámara sobre el código QR </Text>
                            {scanPaquetes.length!=0 &&    
                                <Text numberOfLines={8} style={styles.descText}>Paquetes salientes escaneados: {scanPaquetes.length}</Text>
                            }
                            <Image source={QrCode} style={{margin: 5,height: 80, width: 80}}></Image>
                            <TouchableOpacity onPress={this.activeQR} style={styles.buttonScan}>
                                <View style={styles.buttonWrapper}>
                                <Image source={IconCamera} style={{height: 25, width: 25}}></Image>

                                <Text style={{...styles.buttonTextStyle, color: '#2196f3'}}>Escanear QR</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.sincronizar} style={{ paddingTop: 50, ...styles.buttonScan }}>
                                <View style={styles.buttonWrapper}>

                                <Text style={{...styles.buttonTextStyle, color: '#2196f3'}}>Sincronizar {scanPaquetes.length} paquetes</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    }
                    
                    {/* Horizontal */}


                    {ScanResult &&
                        <Fragment>
                            <Text style={styles.textTitle1}>Result</Text>
                            <View style={ScanResult ? styles.scanCardView : styles.cardView.vertical}>
                                <Text>Type : {result.type}</Text>
                                <Text>Result : {result.data}</Text>
                                <Text numberOfLines={1}>RawData: {result.rawData}</Text>
                                <TouchableOpacity onPress={this.scanAgain} style={styles.buttonScan}>
                                    <View style={styles.buttonWrapper}>
                                        <Image source={IconCamera} style={{height: 36, width: 36}}></Image>
                                        <Text style={{...styles.buttonTextStyle, color: '#2196f3'}}>Click to scan again</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </Fragment>
                    }
                    {scan &&
						<Fragment >
                            <RNCamera
                                        ref={ref => {
                                            this.camera = ref;
                                        }}
                                        defaultTouchToFocus
                                        flashMode={this.state.camera.flashMode}
                                        mirrorImage={false}
                                        onBarCodeRead={this.onBarCodeRead.bind(this)}
                                        onFocusChanged={() => {}}
                                        onZoomChanged={() => {}}
                                        permissionDialogTitle={'Permission to use camera'}
                                        permissionDialogMessage={'We need your permission to use your camera phone'}
                                        style={styles.preview}
                                        type={this.state.camera.type}
                                    />
                                <ImageBackground source={BottomPanel} style={styles.bottomContent}>
                                    <TouchableOpacity style={styles.buttonScan2} 
                                        // onPress={() => this.scanner.reactivate()} 
                                        onLongPress={() => this.setState({ scan: false })}>
                                        <Image source={IconCamera2}></Image>
                                    </TouchableOpacity>
                                </ImageBackground>

						</Fragment >

                    }
                </Fragment>
            </View>
        );
		this.scanner.reactivate(false)

    }
}
const styles = StyleSheet.create({

    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
	scrollViewStyle: {
        flex: 1,
        justifyContent: 'flex-start',
        backgroundColor: '#2196f3'
    },
    header: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        height: '10%',
        paddingLeft: 15,
        paddingTop: 10,
        width: deviceWidth,
    },
    textTitle: {
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center',
        padding: 16,
        color: 'white'
    },
    textTitle1: {
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center',
        padding: 16,
        color: 'white'
    },
    // height: hp('70%'), // 70% of height device screen
    // width: wp('80%')   // 80% of width device screen
    cardView: {
        vertical:{
            width: '80%' ,
            height: '80%',
            alignSelf: 'center',
            justifyContent: 'flex-start',
            alignItems: 'center',
            borderRadius: 10,
            padding: 25,
            marginLeft: 5,
            marginRight: 5,
            backgroundColor: 'white'
        },
        horizontal:{
            
            flexDirection: 'row',
            width:  '80%',
            height:  '80%',
            alignSelf: 'center',
            justifyContent: 'flex-start',
            alignItems: 'center',
            borderRadius: 10,
            padding: 25,
            marginLeft: 5,
            marginRight: 5,
            backgroundColor: 'white'
        }
        
    },
    scanCardView: {
        width: deviceWidth - 32,
        height: deviceHeight / 2,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        padding: 25,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        backgroundColor: 'white'
    },
    buttonWrapper: {
        display: 'flex', 
        flexDirection: 'row',
        alignItems: 'center'
    },
    buttonScan: {
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#258ce3',
        paddingTop: 5,
        paddingRight: 25,
        paddingBottom: 5,
        paddingLeft: 25,
        marginTop: 20
    },
    buttonScan2: {
        marginLeft: deviceWidth / 2 - 50,
        width: 100,
        height: 100,
    },
    descText: {
        padding: 8,
        textAlign: 'center',
        fontSize: 16
    },
    highlight: {
        fontWeight: '700',
    },
    centerText: {
        flex: 1,
        textAlign: 'center',
        fontSize: 18,
        padding: 32,
        color: 'white',
    },
    textBold: {
        fontWeight: '500',
        color: '#000',
    },
    bottomContent: {
       width: deviceWidth,
       height: 120,
    },
    buttonTouchable: {
        fontSize: 21,
        backgroundColor: 'white',
        marginTop: 32,
        width: deviceWidth - 62,
        justifyContent: 'center',
        alignItems: 'center',
        height: 44
    },
    buttonTextStyle: {
        color: 'black',
        fontWeight: 'bold',
    }
	
});

export default connect((state) => {
	return {
        paquetesEscaneados: state.paquetesEscaneados,
		user: state.user,

	}
})(Scan);