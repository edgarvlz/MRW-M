import React, { Fragment,Component  } from 'react';
import { Colors, Alert } from '#/utils';
import { StyleSheet, View,  Text, Image,Dimensions,BackHandler,TouchableOpacity,  ImageBackground, } from 'react-native';
import { connect } from 'react-redux';
import { CommonActions,useIsFocused  } from '@react-navigation/native';
import { OrdenPickup } from '#/services';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { IconBack,IconCamera,IconCamera2,QrCode,BottomPanel } from '#/assets/img';
import { navigationRef } from '#/navigation/root-navigation';

const isPortrait = () => {
    const dim = Dimensions.get('screen');
    return dim.height >= dim.width;
};

var deviceWidth  =  Dimensions.get('window').width ;
var deviceHeight =  Dimensions.get('window').height ;

// console.log("w",deviceWidth,"h",deviceHeight,isPortrait() ?  "Ve" : "Ho",)

class PaqueteSaliente extends Component {
	

	constructor(props) {
	    super(props);
		this.state = {
            scan: false,
            ScanResult: false,
            result: null,
            orientation: isPortrait() ? true : false,
            deviceWidth :    isPortrait() ? Dimensions.get('window').width : Dimensions.get('window').height  ,
            deviceHeight :   isPortrait() ? Dimensions.get('window').height : Dimensions.get('window').width  
        };

	    BackHandler.addEventListener("hardwareBackPress",() => {
	    	Alert.confirm('¿Desea salir del escáner?',() => {
	    		this.props.navigation.goBack();
	    	});
	      	return true;
	    });	
        Dimensions.addEventListener('change', () => {
            this.setState({
                orientation: isPortrait() ?  true : false,
            });
        });
		
	}
	onSuccess = (e) => {
        const navigation = navigationRef?.current;
        navigation.navigate('ResultScan',{
            token: e.data
        });

        this.setState({
            result: e,
            scan: false,
            ScanResult: false
        })
    }
	activeQR = () => {
        this.setState({ scan: true })
    }
    scanAgain = () => {
        this.setState({ scan: false, ScanResult: false })
    }
	
	componentDidMount() {
		
	}

    
    
	
	render() {
        const { scan, ScanResult, result,orientation,deviceWidth,deviceHeight } = this.state
		console.log(orientation)

        return (
            <View style={styles.scrollViewStyle}>
                <Fragment>
                    <View style={styles.header}>
                        <TouchableOpacity 
						onPress={()=> 
									this.props.navigation.goBack()
								
						}>
						<Image source={IconBack} style={{height: 36, width: 36}}></Image>
                        </TouchableOpacity>
                        <Text style={styles.textTitle}></Text>
                    </View>

                    {/*****  Antes de escanear *****/}
                    
                    {/* Vertical */}
                    {!scan && !ScanResult && orientation &&
                      
                        <View style={ styles.cardView.vertical  } >
                            
                            <Image source={IconCamera} style={{height: 36, width: 36}}></Image>
                            <Text numberOfLines={8} style={styles.descText}>Mueva su cámara sobre el código QR</Text>
                            <Image source={QrCode} style={{margin: 20,height: 120, width: 120}}></Image>
                            <TouchableOpacity onPress={this.activeQR} style={styles.buttonScan}>
                                <View style={styles.buttonWrapper}>
                                <Image source={IconCamera} style={{height: 36, width: 36}}></Image>

                                <Text style={{...styles.buttonTextStyle, color: '#2196f3'}}>Escanear QR</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    }
                    {!scan && !ScanResult && !orientation &&
                      
                      <View style={styles.cardView.horizontal  } >
                        <View style={{ flex: .5 , alignItems: 'center'}}>
                          <Image source={IconCamera} style={{height: 36, width: 36}}></Image>
                          <Text numberOfLines={8} style={styles.descText}>Mueva su cámara sobre el código QR</Text>
                        </View>
                        <View style={{ flex: .5 , alignItems: 'center'}}>
                          <Image source={QrCode} style={{margin: 20,height: 100, width: 100}}></Image>
                        </View>
                        <View style={{ flex: .5 , alignItems: 'center'}}>
                        <TouchableOpacity onPress={this.activeQR} style={styles.buttonScan}>
                              <View style={styles.buttonWrapper}>
                              <Image source={IconCamera} style={{height: 36, width: 36}}></Image>

                              <Text style={{...styles.buttonTextStyle, color: '#2196f3'}}>Escanear QR</Text>
                              </View>
                        </TouchableOpacity>
                        </View>
                         

                         

                      </View>
                  }
                    {/* Horizontal */}


                    {ScanResult &&
                        <Fragment>
                            <Text style={styles.textTitle1}>Result</Text>
                            <View style={ScanResult ? styles.scanCardView : styles.cardView.vertical}>
                                <Text>Type : {result.type}</Text>
                                <Text>Result : {result.data}</Text>
                                <Text numberOfLines={1}>RawData: {result.rawData}</Text>
                                <TouchableOpacity onPress={this.scanAgain} style={styles.buttonScan}>
                                    <View style={styles.buttonWrapper}>
                                        <Image source={IconCamera} style={{height: 36, width: 36}}></Image>
                                        <Text style={{...styles.buttonTextStyle, color: '#2196f3'}}>Click to scan again</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </Fragment>
                    }
                    {scan &&
						<Fragment >
							<QRCodeScanner
								reactivate={true}
								showMarker={false}
								ref={(node) => { this.scanner = node }}
								onRead={this.onSuccess}
								topContent={
									<Text style={styles.centerText}>
									Mueva su cámara sobre{"\n"}el código QR
									</Text>
								}
								bottomContent={
									<View>
										<ImageBackground source={BottomPanel} style={styles.bottomContent}>
											<TouchableOpacity style={styles.buttonScan2} 
												onPress={() => this.scanner.reactivate()} 
												onLongPress={() => this.setState({ scan: false })}>
												<Image source={IconCamera2}></Image>
											</TouchableOpacity>
										</ImageBackground>
									</View>
								}
							/>
						</Fragment >

                    }
                </Fragment>
            </View>
        );
		this.scanner.reactivate(false)

    }
}
const styles = StyleSheet.create({
	scrollViewStyle: {
        flex: 1,
        justifyContent: 'flex-start',
        backgroundColor: '#2196f3'
    },
    header: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        height: '10%',
        paddingLeft: 15,
        paddingTop: 10,
        width: deviceWidth,
    },
    textTitle: {
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center',
        padding: 16,
        color: 'white'
    },
    textTitle1: {
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center',
        padding: 16,
        color: 'white'
    },
    // height: hp('70%'), // 70% of height device screen
    // width: wp('80%')   // 80% of width device screen
    cardView: {
        vertical:{
            width: '80%' ,
            height: '60%',
            alignSelf: 'center',
            justifyContent: 'flex-start',
            alignItems: 'center',
            borderRadius: 10,
            padding: 25,
            marginLeft: 5,
            marginRight: 5,
            backgroundColor: 'white'
        },
        horizontal:{
            
            flexDirection: 'row',
            width:  '80%',
            height:  '80%',
            alignSelf: 'center',
            justifyContent: 'flex-start',
            alignItems: 'center',
            borderRadius: 10,
            padding: 25,
            marginLeft: 5,
            marginRight: 5,
            backgroundColor: 'white'
        }
        
    },
    scanCardView: {
        width: deviceWidth - 32,
        height: deviceHeight / 2,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        padding: 25,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        backgroundColor: 'white'
    },
    buttonWrapper: {
        display: 'flex', 
        flexDirection: 'row',
        alignItems: 'center'
    },
    buttonScan: {
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#258ce3',
        paddingTop: 5,
        paddingRight: 25,
        paddingBottom: 5,
        paddingLeft: 25,
        marginTop: 20
    },
    buttonScan2: {
        marginLeft: deviceWidth / 2 - 50,
        width: 100,
        height: 100,
    },
    descText: {
        padding: 16,
        textAlign: 'center',
        fontSize: 16
    },
    highlight: {
        fontWeight: '700',
    },
    centerText: {
        flex: 1,
        textAlign: 'center',
        fontSize: 18,
        padding: 32,
        color: 'white',
    },
    textBold: {
        fontWeight: '500',
        color: '#000',
    },
    bottomContent: {
       width: deviceWidth,
       height: 120,
    },
    buttonTouchable: {
        fontSize: 21,
        backgroundColor: 'white',
        marginTop: 32,
        width: deviceWidth - 62,
        justifyContent: 'center',
        alignItems: 'center',
        height: 44
    },
    buttonTextStyle: {
        color: 'black',
        fontWeight: 'bold',
    }
	
});

export default connect((state) => {
	return {
        
	}
})(PaqueteSaliente);