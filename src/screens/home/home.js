import React from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Image, Dimensions, ScrollView,TouchableWithoutFeedback  } from 'react-native';
import { Colors, Fonts,DataFake,Lenguaje,Alert } from '#/utils';
import * as Animatable from 'react-native-animatable';

import { 
	HomeManagerIcon, 
	CarManagerIcon, 
	SearchIcon, 
	EyeIcon, 
	CheckOutlineIcon,
	CheckIcon,
	ReportIcon,
	TimeIcon,
	CogIcon,
    HomeClientIcon
	
} from '#/assets/icons';
import { LogoMensajero } from '#/assets/img';
import { ScreenContainer, Input, Modal } from '#/components';
import { connect } from 'react-redux';
import ModalAgencia from './modal-agencia';
import { Header } from '#/navigation';


class Home extends React.Component {
	state = {
		form: {
			search: ''
		},
		timeout: null,
		loading: true,
		visible: false,
		textos: 1,
		visible_codigo_agencia: false
	}


	componentDidMount() {
		this.props.navigation.setParams({
			title: 'Inicio',
			showMenu: true
		});
		
		this.load();

		this.verificarScaner();
		
	}

	componentWillUnmount() {
		this.verificarScaner();

	}

	verificarScaner = () => {
		if (this.props.paquetesEscaneados.listEntrante.length != 0) {
			Alert.confirm2('Tienes paquetes por sincronizar, ¿Deseas Continuar?', (r) => {
				if (r == "cancelar") {
					this.props.dispatch({
						type: 'RESET_SCANER',
						payload: null
					});
				} else if (r == "aceptar") {
					this.props.navigation.navigate('ScanEntrante');
				}
			});
		} else if (this.props.paquetesEscaneados.listSaliente.length != 0) {
			Alert.confirm2('Tienes paquetes por sincronizar, ¿Deseas Continuar?', (r) => {
				if (r == "cancelar") {
					this.props.dispatch({
						type: 'RESET_SCANER',
						payload: null
					});
				} else if (r == "aceptar") {
					this.props.navigation.navigate('ScanSaliente');
				}
			});
		}
	}

	textosIdioma = (value) => {
		if(value==1){
			this.state.textos= Lenguaje.prueba_es	
		}else{
			this.state.textos= Lenguaje.prueba_en	
		}
	}

	reset = async () => {
	
	}

	load = async () => {
	
		
	}

	change = (value,target) => {
		this.setState({
			form: {
				...this.state.form,
				[target]: value
			}
		});
	}
	
	onClose = () => {
		this.setState({
			visible: false,
			order_id: null
		});
	}

	openEntrada = () => {
		//this.props.navigation.navigate('PaEntrante', { screen: 'ScanEntrante' });
		this.props.navigation.navigate('ScanEntrante');
	}	
	
	openSaliente = () => {
		//this.props.navigation.navigate('PaEntrante', { screen: 'ScanEntrante' });
		this.props.navigation.navigate('ScanSaliente');
	}	
	// openModalAgencia = () => {
	// 	this.setState({
	// 		visible_codigo_agencia: true
	// 	});
	// }	
	
	closeModalAgencia = (e) => {

			this.setState({
				visible_codigo_agencia: false,
				loading: true
			},() => {
				this.setState({
					loading: false
				});			
			});

			if(e == 'no_codigo') {

			}else{
				this.props.navigation.navigate('PaEntrante', { screen: 'ScanEntrante' });
			}
	
	}
	 	
	paquetesEntrantes = () => {

	}
	render() {
		const { visible_codigo_agencia } = this.state;
		
		return (
			
			<ScreenContainer >
				{
					visible_codigo_agencia && (
						<Modal 
							withoutClose={ true }
							containerStyle={ styles.modal }
							onClose={ this.closeModalAgencia }>
							<ModalAgencia
								onClose={ this.closeModalAgencia }
								vehicle={ this.state.vehicle }
							/>
						</Modal>
					)
				}
				<Header
					navigation={this.props.navigation}
					params={{
						title: 'Inicio',
						showMenu:true,
						containerStyle: {
							backgroundColor: '#F6F6F6'
						},
						menuOn: true,
					}}
				/>
				<Animatable.View 
					style={styles.blackContainer} 
					animation="fadeInUp" 
					duration={1500} 
					delay={1000}
				>
					<View style={{flex: 5, ...styles.boxOptions}}>
					<TouchableOpacity onPress={ this.openEntrada }>

						{/*   this.props.navigation.navigate('AdminHome',{
							  	index: 0,
							  	routes: [{ name: 'PaEntrante' }],
							  });
							 this.props.navigation.navigate('PaEntrante', { screen: 'ScanEntrante' });; 
						*/}
							 

						
							<View style={[styles.item]}>
								<View style={[styles.itemColumn]}>
									<Text style={styles.itemTextTitulo}>
										Paquetes Entrantes
									</Text>
									<View style={[styles.itemRow]}>
										<Image source={HomeManagerIcon} style={[styles.icon]}/>
										<Text style={styles.textActive}>
										</Text>
									</View>
								</View>
							</View>
						</TouchableOpacity>
					</View>
					<View style={{ flex: 5, ...styles.boxOptions}}>
					<TouchableOpacity onPress={ this.openSaliente }>

							<View style={[styles.item]}>
								<View style={[styles.itemColumn]}>
									<Text style={styles.itemTextTitulo}>
									Paquetes Salientes
									</Text>
									<View style={[styles.itemRow]}>
										<Image source={HomeManagerIcon} style={[styles.icon]}/>
										<Text style={styles.textActive}>
										</Text>
									</View>
								</View>
							</View>
						</TouchableOpacity>
					</View>
				</Animatable.View>
			</ScreenContainer>
		)		
	}
}

const styles = StyleSheet.create({
	ScreenContainer:{
		backgroundColor: Colors.gray,
	},
	boxOptions:{
		width: '80%',
		
	},
	blackContainer: {
		backgroundColor: Colors.gray,
		width: '100%',
        height: '100%',
		alignItems: 'center',
        flexDirection: 'column',
		alignContent :'center',
		paddingTop:10, 
	},
	whiteContainer: {
		backgroundColor: Colors.sky,
        borderRadius:20,
        marginRight:20,
        marginLeft:20,
        marginTop:10,
	},
	icon: {
		width: 50,
		height: 50,
		tintColor: Colors.secondary
	},
	iconActive: {
		tintColor: Colors.white
	},
	item: {
		alignSelf: 'center',
		height:120,
		backgroundColor: Colors.white,
		width: '100%',
		marginLeft:'10%',
		marginRight:'10%',
        maxWidth:'100%',
		flexDirection: 'row',
		alignItems: 'center',
		borderRadius: 10,
		justifyContent: 'center'
	},
	itemColumn: {
		flexDirection: 'column',
		alignItems: 'center',
		marginRight:30,
		marginLeft:30,

	},
	itemRow: {
		flexDirection: 'row',
	},
	itemActive: {
		backgroundColor: Colors.primary
	},
	text: {
		color: Colors.black,
		textAlign: 'center',
		marginTop: 10
	},
	textActive: {
		color: Colors.gray2,
		fontSize: 15,
	},
	itemTextTitulo: {
		color: Colors.black,
		fontSize: 20,
		fontWeight: "bold"
	},
	
	inputContainer: {
		backgroundColor: Colors.gray,
		padding: 10,
		borderTopRightRadius: 20,
		borderTopLeftRadius: 20,
	},
	input: {
		marginBottom: 0
	},
	itemVehicle: {
		padding: 10,
		flexDirection: 'row',
		borderBottomWidth: 1,
		borderBottomColor: Colors.gray2
	},
	eye: {
		tintColor: Colors.blue,
		width: 30,
		height: 30,
		resizeMode: 'contain'
	},
	iconCheck: {
		width: 30,
		height: 30,
		resizeMode: 'contain'
	},
	textVehicle: {
		fontSize: 11
	},
	statusVehicle: {
		textTransform: 'uppercase',
		color: Colors.blue,
		fontSize: 10
	},
	filter: {
		fontSize: 12, 
		color: Colors.primary,
		marginLeft: 10,
		marginTop: 5,
		marginBottom: 5
	},
	barGray: {
		backgroundColor: Colors.red,
		flexDirection: 'row',
		padding: 10,
		alignItems: 'center',
		borderTopRightRadius: 20,
		borderTopLeftRadius: 20,
		borderBottomColor: Colors.gray2
	},
	iconGas: {
		width: 25,
		height: 25,
		resizeMode: 'contain',
		marginRight: 10
	},
	crashBar: {
		minHeight: '20%',
		flex: 1
	},
	ordersBar: {
		minHeight: '20%',
		flex: 1
	},
	noVehicle: {
		textAlign: 'center',
		marginTop: 10,
		marginBottom: 10
	},
	iconAvailable: {
		width: 25,
		height: 25,
		resizeMode: 'contain',
		tintColor: Colors.primary
	},
	itemAvailable: {
		flexDirection: 'row',
		padding: 10
	},
	itemVehicleBorder: {
		borderBottomWidth: 1,
		borderBottomColor: Colors.gray2
	},
	label: {
		fontSize: 12
	},
	labelCar: {
		color: Colors.primary,
		fontSize: 12
	},
	date: {
		textAlign: 'right',
		fontSize: 12,
		color: Colors.gray2
	},
	modal: {
		padding: 0,
		width: '90%'
	},
	imageStyles: {
		width: '70%',
		height: 100,
		resizeMode: 'contain',
		marginTop: 0,

	},
});

export default connect(state => {
	
	return {
		user: state.user,
		ordenes: state.ordenes,
        paquetesEscaneados: state.paquetesEscaneados,
		codigoAgencia: state.codigoAgencia,
	}
	
})(Home);