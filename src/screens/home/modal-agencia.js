import React from 'react';
import { View, StyleSheet, TouchableOpacity, Text, ScrollView, Dimensions } from 'react-native';
import { Icon, Button, DatePicker, Input } from '#/components';
import { Colors, Fonts, Globals, Alert, Socket, SocketEvents } from '#/utils';
import { DistanceService, VehicleService } from '#/services';
import moment from 'moment';
import { connect } from 'react-redux';


class ModalAgencia extends React.Component {

	state = {
		form: {
			date: moment().format('DD/MM/YYYY'),
			until: ''
		},
		vehicle: null
	}

	componentDidMount() {
		//this.load();

		this.props.dispatch({
			type: 'SET_CODIGO_AGENCIA',
			payload: null
		});
		this.state.form.until = ''

	}

	load = async () => {
		const res = await VehicleService.get({
			user_id: this.props.user.id
		});
		this.setState({
			vehicle: res.vehicle
		});
	}

	change = (value,target) => {
		this.setState({
			form: {
				...this.state.form,
				[target]: value
			}
		});
	}

	closeX = () => {
		this.props.onClose('no_codigo');
		
	}

	submit = async () => {
		if(this.state.form.until == ''){
			alert("Ingrese un número de agencia")
		}
		else{
			this.props.dispatch({
				type: 'SET_CODIGO_AGENCIA',
				payload: {codigo_agencia:this.state.form.until}
			});
			this.props.onClose(this.state.form.until);
		}
	}
	
	render() {
		const { vehicle } = this.state;

		return (
			<React.Fragment>
				<View style={ styles.header }>
					<View style={{ flex: .8}}>
						<Text style={ styles.textHeader }>Paquetes entrantes</Text>
					</View>					
					<View style={{ marginLeft: 10, flex: .2, alignItems: 'flex-end' }}>
						<TouchableOpacity onPress={ this.closeX }>
							<Icon name="ios-close" color={ Colors.white } size={ 25 } />
						</TouchableOpacity>
					</View>					
				</View>
				<View style={ styles.containerForm }>
			
						<ScrollView>
							<View style={ styles.containerScroll }>
								<Text style={ [styles.label,{ marginTop: 20 }] }>Código de agencia</Text>
								<Input
										onChangeText={ value => {
											if (Globals.validateInteger(value) || value == '') {
												this.change(value,'until');
											}
										} }
										value={ this.state.form.until }
										// textRight="Km"
										keyboardType="numeric"
										style={ styles.input }
									/>
									
								<View style={ styles.buttonContainer }>
									<Button 
										onPress={ this.submit }
										textStyle={ styles.buttonText }
										title="Agregar"
										style={ styles.button }
									/>
								</View>
							</View>
						</ScrollView>
						
				</View>
			</React.Fragment>
		)		
	}
}

const styles = StyleSheet.create({
	header: {
		backgroundColor: Colors.secondary,
		padding: 10,
		flexDirection: 'row',
		borderTopRightRadius: 10,
		borderTopLeftRadius: 10,
		width: '100%'
	},
	textHeader: {
		color: Colors.white,
		textTransform: 'uppercase',
		fontSize: 12,
		marginTop: 4
	},
	containerForm: {
		backgroundColor: Colors.white,
		width: '100%',
		minHeight: 50,
		borderBottomRightRadius: 10,
		borderBottomLeftRadius: 10,
		maxHeight: '90%'
	},
	containerScroll: {
		padding: 15
	},
	input: {
		backgroundColor: Colors.gray
	},
	label: {
		marginTop: 10,
		fontSize: 12
	},
	buttonText: {
		color: Colors.white,
		textAlign: 'center',
		fontSize: 12
	},
	buttonContainer: {
		alignItems: 'center'
	},
	button: {
		minWidth: 130,
		height: 35,
		marginTop: 10,
		marginBottom: 10
	}
});

export default connect(state => {
	return {
		user: state.user,
		codigoAgencia: state.codigoAgencia
	}
})(ModalAgencia);