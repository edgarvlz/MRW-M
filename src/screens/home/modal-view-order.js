import React from 'react';
import { View, StyleSheet, Text, ScrollView, TouchableOpacity } from 'react-native';
import { Colors, Fonts } from '#/utils';
import { OrderService } from '#/services';
import { Icon } from '#/components';
import moment from 'moment';

class ModalViewOrder extends React.Component {
	state = {
		order: null
	}

	componentDidMount() {
		this.load();
	}

	load = async () => {
		
		this.setState({
			order: this.props.order
		});

		
	}
	listProductos = (orden) => {

	}

	render() {
		const { order } = this.state;

		return (
			<React.Fragment>
				<View style={ styles.header }>
					<View style={{ flex: .8}}>
						<Text style={ styles.textHeader }>Lista de productos </Text>
					</View>					
					<View style={{ marginLeft: 10, flex: .2, alignItems: 'flex-end' }}>
						<TouchableOpacity onPress={ this.props.onClose }>
							<Icon name="ios-close" color={ Colors.white } size={ 25 } />
						</TouchableOpacity>
					</View>					
				</View>
				<View style={ styles.containerForm }>
					{
						order && (
							<ScrollView>
								<View style={ styles.containerScroll }>
									<Text style={ styles.date }>Fecha de craeción: { moment(order?.created_at).format('DD/MM/YYYY') }</Text>
									
									
									{order?.productos.map((data, key) => {
										return (
											<View style={ [styles.item, styles.itemBorder] }>
												<Text style={ styles.textItem }>{ data }</Text>
											</View>
										);
									})}
								</View>
							</ScrollView>
						)
					}
				</View>
			</React.Fragment>
		)		
	}
}

const styles = StyleSheet.create({
	header: {
		backgroundColor: Colors.primary,
		padding: 10,
		flexDirection: 'row',
		borderTopRightRadius: 10,
		borderTopLeftRadius: 10,
		width: '100%'
	},
	textHeader: {
		color: Colors.white,
		textTransform: 'uppercase',
		fontSize: 12,
		marginTop: 4
	},
	containerForm: {
		backgroundColor: Colors.white,
		width: '100%',
		minHeight: 50,
		borderBottomRightRadius: 10,
		borderBottomLeftRadius: 10,
		maxHeight: '90%'
	},
	containerScroll: {},
	itemBorder: {
		borderBottomColor: Colors.gray2,
		borderBottomWidth: 1,
	},
	item: {
		padding: 8,
	},
	date: {
		padding: 10,
		fontSize: 14,
		textAlign: 'right'
	}
});

export default ModalViewOrder;