import React, { Fragment,Component  } from 'react';
import { Colors, Alert } from '#/utils';
import { StyleSheet, View,  Text, Image,Dimensions,BackHandler,TouchableOpacity,  ImageBackground,ActivityIndicator } from 'react-native';
import {  Modal,ScreenContainer } from '#/components';
import { connect } from 'react-redux';
import { OrdenPickup } from '#/services';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { IconBack,IconCamera,IconCamera2,QrCode,BottomPanel } from '#/assets/img';
import moment from 'moment';
import { navigationRef } from '#/navigation/root-navigation';
import { CommonActions } from '@react-navigation/native';
import ModalViewOrder from './modal-view-order';


const deviceWidth = Dimensions.get('screen').width;
const deviceHeight = Dimensions.get('screen').height;


class Scan extends Component {
	

	constructor(props) {
	    super(props);
		this.state = {
            scan: false,
            ScanResult: false,
            result: null,
            orden: null,
            visible: false,
            typeScan: this.props.typeScanner

        };
	    BackHandler.addEventListener("hardwareBackPress",() => {
	    	Alert.confirm('¿Desea salir del escáner?',() => {
	    		this.props.navigation.goBack();
	    	});
	      	return true;
	    });	
	
	}
	onSuccess = (e) => {

		const check = e.data;
		
		this.setState({
			result: e,
			scan: false,
			ScanResult: true
		})
		
		if (check === 'http') {
			Linking.openURL(e.data).catch(err => console.error('An error occured', err));
		} else {
			this.setState({
				result: e,
				scan: false,
				ScanResult: true
			})
		}
    }
	activeQR = () => {
        this.setState({ scan: true })
    }
    scanAgain = () => {
        this.setState({ scan: false, ScanResult: false })
    }
	
	componentDidMount() {
		this.load()
	}
    load = async (withoutLoading = true, reload = false) => {
       
		const res = await OrdenPickup.consultar({
			token: this.props.route.params.token,
		});

        if(res.code=='QR_FALSE'){
            Alert.notification(res.menssage,() => {

	    		this.props.navigation.goBack();

	    	});
        }else{
            this.setState({
                orden: res.orden,
                ScanResult: true,
                typeScan: this.props.typeScanner
               
            });
            //Verificar si existe en redux
            if(this.props.ordenes.listOrdenes.find(item => item.pickup_id === res.orden.pickup_id)){

            }else{
                
                this.props.dispatch({
                      type: 'PREPEND_ORDENES',
                      payload: res.orden
                });
            }
           
        }
        

	}
    entregarAgencia = async (withoutLoading = true, reload = false) => {
        var menssage = '';
        if(this.state.typeScan == 2){
            menssage = 'Entregado a Agencia.';
        }else{
            menssage = 'Entregado a Comprador.';
            
        }
        const navigation = navigationRef?.current;
		const res = await OrdenPickup.status({
			token: this.props.route.params.token,
            status: this.state.typeScan
		});
        if(res.estatus==true){

            Alert.notification(menssage,() => {

                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [{
                            name: 'AdminHome'
                        }]
                    })
                )

            })
			
		 }
        if(res.status==true){
            Alert.notification('Entregado a Agencia.',() => {
                this.entregarAgencia()
            })
        }
    }

    onClose = () => {
		this.setState({
			visible: false,
			order_id: null
		});
	}
    
    toAgencia = () => {
        if(res.status==true){
            Alert.notification('Entregado a Agencia.',() => {
                this.entregarAgencia()
            })
        }
	}
	

	render() {
        const { scan, ScanResult, result,orden,visible,typeScan } = this.state
        return (
            <ScreenContainer>
                {
                    visible && (
                        <Modal 
                            withoutClose={ true }
                            containerStyle={ styles.modal }
                            onClose={ this.onClose }>
                            <ModalViewOrder
                                onClose={ this.onClose }
                                order={ orden }
                            />
                        </Modal>
                    )
                }
                <View style={styles.scrollViewStyle}>
                    <Fragment>
                        <View style={styles.header}>
                            <TouchableOpacity 
                            onPress={()=> 
                                this.props.navigation.goBack()
                            }>
                            <Image source={IconBack} style={{height: 36, width: 36}}></Image>
                            </TouchableOpacity>
                            <Text style={styles.textTitle}></Text>
                        </View>
                        {!scan && !ScanResult &&
                            <View style={styles.cardView} >
                                <Image source={IconCamera} style={{height: 36, width: 36}}></Image>
                                <Text numberOfLines={8} style={styles.descText}>Analizando QR</Text>
                                <ActivityIndicator size="large" color={Colors.primary} />

                            </View>
                        }
                        {ScanResult &&
                            <Fragment>
                                <Text style={styles.textTitle1}>Datos de orden</Text>
                                <View style={ScanResult ? styles.scanCardView : styles.cardView}>
                                    <Text style={styles.textResults}>Vendedor : {orden.vendedor} {orden.documento_vendedor}</Text>
                                    <Text style={styles.textResults}>Comprador : {orden.comprador} {orden.documento_comprador}</Text>
                                    <TouchableOpacity onPress={()=>
                                       
                                        this.setState({
                                            visible: true,
                                        })
                                        
                                        }>
                                        <Text style={styles.textResults1}>Ver productos... </Text>
                                    </TouchableOpacity>
                                    
                                    <Text style={styles.textResults}>Fecha de creación : {moment(orden.created_at).format('YYYY-MM-DD')}</Text>
                                    
                                    <TouchableOpacity onPress={()=>

                                        {
                                            if(typeScan==2){
                                                Alert.confirm('¿Marcar orden como entregada a Agencia?',() => {
                                                    this.entregarAgencia()
                                                })
                                            } else{
                                                Alert.confirm('¿Marcar orden como entregada a Comprador?',() => {
                                                    this.entregarAgencia()
                                                })
                                            }
                                        }
                                    
                                        
                                            
                                        
                                        } style={{...styles.buttonScan}}>
                                        <View style={styles.buttonWrapper}>
                                            <Text style={{...styles.buttonTextStyle, color: '#6c0055'}}>Continuar</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </Fragment>
                        }
                        {scan &&
                            <Fragment >
                                <QRCodeScanner
                                    reactivate={true}
                                    showMarker={false}
                                    ref={(node) => { this.scanner = node }}
                                    onRead={this.onSuccess}
                                    topContent={
                                        <Text style={styles.centerText}>
                                        Mueva su cámara sobre{"\n"}el código QR
                                        </Text>
                                    }
                                    bottomContent={
                                        <View>
                                            <ImageBackground source={BottomPanel} style={styles.bottomContent}>
                                                <TouchableOpacity style={styles.buttonScan2} 
                                                    onPress={() => this.scanner.reactivate()} 
                                                    onLongPress={() => this.setState({ scan: false })}>
                                                    <Image source={IconCamera2}></Image>
                                                </TouchableOpacity>
                                            </ImageBackground>
                                        </View>
                                    }
                                />
                            </Fragment >

                        }
                    </Fragment>
                </View>
            </ScreenContainer>
        );
		this.scanner.reactivate(false)

    }
}
const styles = StyleSheet.create({
    textResults: {
        paddingTop:3,
    },
    textResults1: {
        paddingTop:3,
        fontWeight: "bold"
    },
	scrollViewStyle: {
        flex: 1,
        justifyContent: 'flex-start',
        backgroundColor: '#6c0055'
    },
    header: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        height: '10%',
        paddingLeft: 15,
        paddingTop: 10,
        width: deviceWidth,
    },
    textTitle: {
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center',
        padding: 16,
        color: 'white'
    },
    textTitle1: {
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center',
        padding: 16,
        color: 'white'
    },
    cardView: {
      
        alignSelf: 'center',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderRadius: 10,
        padding: 25,
        marginLeft: 5,
        marginRight: 5,
        marginTop: '10%',
        backgroundColor: 'white'
    },
    scanCardView: {
        width: deviceWidth - 32,
        height: deviceHeight / 2,
        alignSelf: 'center',
        justifyContent: 'center',
        // alignItems: 'center',
        borderRadius: 10,
        padding: 25,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        backgroundColor: 'white'
    },
    buttonWrapper: {
        display: 'flex', 
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonScan: {
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#6c0055',
        paddingTop: 5,
        paddingRight: 25,
        paddingBottom: 5,
        paddingLeft: 25,
        marginTop: 20
    },
    buttonScan2: {
        marginLeft: deviceWidth / 2 - 50,
        width: 100,
        height: 100,
    },
    descText: {
        padding: 16,
        textAlign: 'center',
        fontSize: 16
    },
    highlight: {
        fontWeight: '700',
    },
    centerText: {
        flex: 1,
        textAlign: 'center',
        fontSize: 18,
        padding: 32,
        color: 'white',
    },
    textBold: {
        fontWeight: '500',
        color: '#000',
    },
    bottomContent: {
       width: deviceWidth,
       height: 120,
    },
    buttonTouchable: {
        fontSize: 21,
        backgroundColor: 'white',
        marginTop: 32,
        width: deviceWidth - 62,
        justifyContent: 'center',
        alignItems: 'center',
        height: 44
    },
    buttonTextStyle: {
        color: 'black',
        fontWeight: 'bold',
    }
	
});

export default connect((state) => {
	return {
		user: state.user,
        ordenes: state.ordenes,
        typeScanner: state.typeScanner
    }
})(Scan);