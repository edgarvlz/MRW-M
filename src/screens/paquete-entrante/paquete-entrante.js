import React from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Image, Dimensions, ScrollView,TouchableWithoutFeedback,BackHandler  } from 'react-native';
import { Colors, Fonts,DataFake,Lenguaje } from '#/utils';
import * as Animatable from 'react-native-animatable';

import { 
	HomeManagerIcon, 
	
} from '#/assets/icons';
import { LogoMensajero } from '#/assets/img';
import { ScreenContainer, Input, Modal } from '#/components';
import { connect } from 'react-redux';
import { VehicleService } from '#/services';
import ModalViewOrder from '../home/modal-view-order';
import { Header } from '#/navigation';



class PaqueteEntrante extends React.Component {
	state = {
		form: {
			search: ''
		},
		order_id: null,
		textos: 1,
	}
	
	componentDidMount() {
		// this.props.navigation.replace('Scan');

		this.load();

		if (!this.subscriber) {
			this.subscriber = this.props.navigation.addListener("focus", async () => {
				this.setState({
					loading: true
				},() => {
					this.setState({
						loading: false
					});			
				});
			});
		}
		//  BackHandler.addEventListener("hardwareBackPress",() => {
	    // 		this.props.navigation.goBack();
	    //   	    return true;
	    // });	
	}
	

	reset = async () => {
		await this.setState({
			form: {
				search: ''
			},
			order_id: null,
			textos: 1,
		});
		this.load();
	}

	load = async (withoutLoading = false) => {

	}
	
	
	abrirScanerAgencia = (e) => {
		this.props.dispatch({
				type: 'SET_SCANNER',
				payload: e
		});
		this.props.navigation.navigate('LectorQR');
	}
	 	
	render() {

		return (
			<ScreenContainer >
				<Header
							navigation={ this.props.navigation }
							params={ {
								title: 'Paquete Entrante',
								showMenu:true,
								containerStyle: {
									backgroundColor: '#F6F6F6'
								},
								menuOn: true,
							} } />
			
				<View style={ styles.blackContainer } >
					<Animatable.View style={{ flex: .1, ...styles.boxOptions}} 
						animation="fadeInLeft" 
						duration={1000} 
						delay={1000}>
						<TouchableOpacity onPress={ () => {
								this.props.navigation.replace('ScanEntrante');
						} }>
							<View style={ [styles.item  ] }>
								<View style={ [styles.itemColumn  ] }>
									<View style={ [styles.itemRow  ] }>
										<Image source={ HomeManagerIcon } style={ [styles.icon ] } />
										<Text style={  styles.textActive  }>Escanear Manifiesto</Text>
									</View>
								</View>
							</View>
						</TouchableOpacity>
					</Animatable.View>
					<Animatable.View style={{ flex: .1, ...styles.boxOptions}}
						animation="fadeInRight" 
						duration={1000} 
						delay={1000}>
						<TouchableOpacity onPress={ () => {
								
						} }>
							<View style={ [styles.item  ] }>
								<View style={ [styles.itemColumn  ] }>
									<View style={ [styles.itemRow  ] }>
										<Image source={ HomeManagerIcon } style={ [styles.icon ] } />
										<Text style={  styles.textActive  }>Escanear Paquetes</Text>
									</View>
								</View>
							</View>
						</TouchableOpacity>
					</Animatable.View>
					
				</View>
				
			</ScreenContainer>
		)		
	}
}

const styles = StyleSheet.create({
	ScreenContainer:{
		backgroundColor: Colors.gray,
	},
	boxOptions:{
		width: '80%',
		
	},
	blackContainer: {
		backgroundColor: Colors.gray,
		width: '100%',
        height: '100%',
		alignItems: 'center',
        flexDirection: 'column',
		alignContent :'center',
		paddingTop:10, 
	},
	whiteContainer: {
		backgroundColor: Colors.sky,
        borderRadius:20,
        marginRight:20,
        marginLeft:20,
        marginTop:10,
	},
	icon: {
		width: 50,
		height: 50,
		tintColor: Colors.secondary
	},
	iconActive: {
		tintColor: Colors.white
	},
	item: {
		alignSelf: 'center',
		height:80,
		backgroundColor: Colors.white,
		width: '100%',
		marginLeft:'10%',
		marginRight:'10%',
        maxWidth:'100%',
		flexDirection: 'row',
		alignItems: 'center',
		borderRadius: 10,
		justifyContent: 'center'
	},
	itemColumn: {
		flexDirection: 'column',
		alignItems: 'center',
		marginRight:30,
		marginLeft:30,

	},
	itemRow: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	itemActive: {
		backgroundColor: Colors.primary
	},
	text: {
		color: Colors.black,
		textAlign: 'center',
		marginTop: 10
	},
	textActive: {
		color: Colors.gray2,
		fontSize: 15,
	},
	itemTextTitulo: {
		color: Colors.black,
		fontSize: 20,
		fontWeight: "bold"
	},
	
	inputContainer: {
		backgroundColor: Colors.gray,
		padding: 10,
		borderTopRightRadius: 20,
		borderTopLeftRadius: 20,
	},
	input: {
		marginBottom: 0
	},
	itemVehicle: {
		padding: 10,
		flexDirection: 'row',
		borderBottomWidth: 1,
		borderBottomColor: Colors.gray2
	},
	eye: {
		tintColor: Colors.blue,
		width: 30,
		height: 30,
		resizeMode: 'contain'
	},
	iconCheck: {
		width: 30,
		height: 30,
		resizeMode: 'contain'
	},
	textVehicle: {
		fontSize: 11
	},
	statusVehicle: {
		textTransform: 'uppercase',
		color: Colors.blue,
		fontSize: 10
	},
	filter: {
		fontSize: 12, 
		color: Colors.primary,
		marginLeft: 10,
		marginTop: 5,
		marginBottom: 5
	},
	barGray: {
		backgroundColor: Colors.red,
		flexDirection: 'row',
		padding: 10,
		alignItems: 'center',
		borderTopRightRadius: 20,
		borderTopLeftRadius: 20,
		borderBottomColor: Colors.gray2
	},
	iconGas: {
		width: 25,
		height: 25,
		resizeMode: 'contain',
		marginRight: 10
	},
	crashBar: {
		minHeight: '20%',
		flex: 1
	},
	ordersBar: {
		minHeight: '20%',
		flex: 1
	},
	noVehicle: {
		textAlign: 'center',
		marginTop: 10,
		marginBottom: 10
	},
	iconAvailable: {
		width: 25,
		height: 25,
		resizeMode: 'contain',
		tintColor: Colors.primary
	},
	itemAvailable: {
		flexDirection: 'row',
		padding: 10
	},
	itemVehicleBorder: {
		borderBottomWidth: 1,
		borderBottomColor: Colors.gray2
	},
	label: {
		fontSize: 12
	},
	labelCar: {
		color: Colors.primary,
		fontSize: 12
	},
	date: {
		textAlign: 'right',
		fontSize: 12,
		color: Colors.gray2
	},
	modal: {
		padding: 0,
		width: '90%'
	},
	imageStyles: {
		width: '70%',
		height: 100,
		resizeMode: 'contain',
		marginTop: 0,

	},
});

export default connect(state => {
	
	return {
		user: state.user,
		ordenes: state.ordenes,
		lenguaje: state.lenguaje,
	}
})(PaqueteEntrante);