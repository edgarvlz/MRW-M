import React, { useEffect } from 'react';
import { useFonts } from 'expo-font';
import SplashScreen from 'react-native-splash-screen';
import 'react-native-gesture-handler';
import { store, persistor } from './src/store';
import { PersistGate } from 'redux-persist/integration/react';
import NavigationContainer from '#/navigation';
import { Modal } from '#/components';
import { useSelector, useDispatch, Provider } from 'react-redux';
import { ActivityIndicator, View, KeyboardAvoidingView, PermissionsAndroid,StyleSheet, Text,LogBox, Platform } from 'react-native';
import { Colors,Fonts } from '#/utils';
import { RootSiblingParent } from 'react-native-root-siblings';
import GeneralStatusBarColor from "react-native-general-statusbar";
import { enableScreens } from 'react-native-screens';
import {
  CameraDeviceFormat,
  CameraRuntimeError,
  FrameProcessorPerformanceSuggestion,
  PhotoFile,
  sortFormats,
  useCameraDevices,
  useFrameProcessor,
  VideoFile,
  Camera
} from 'react-native-vision-camera';



LogBox.ignoreAllLogs(
  [
    "exported from 'deprecated-react-native-prop-types'.",
    'Require cycle:',
    "ViewPropTypes will be removed",
    "ColorPropType will be removed",
  ])
  

  enableScreens(false);
  const AppContainer = () => {
  var varialbe = 0;
  const loading = useSelector(state => state.loading);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({
      type: 'QUIT_LOADING'
    });

    // const customTextProps = { 
    //   style: { 
    //     fontFamily: 'Montserrat-Regular'
    //   }
    // }

    // setCustomText(customTextProps);
    if (Platform.OS == 'android') {
      
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA);
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE);

    }
  },[]);

  return (
    <React.Fragment>
      {
       loading && (
         <Modal withoutClose={ true }>
           <View style={ { padding: 20 } }>
             <ActivityIndicator size={ 40 } color={ Colors.primary } />
           </View>           
         </Modal>
       )
     }
     <NavigationContainer>
       
     </NavigationContainer> 
    </React.Fragment>
  )
}

const AvoidingView = (props) => {
  if (Platform.OS == 'ios') {
    return (
       <KeyboardAvoidingView behavior='padding' enabled style={ { flex: 1 } }>
         { props.children }
       </KeyboardAvoidingView>
    )   
  }
  else {
    return (
       <KeyboardAvoidingView style={ { flex: 1 } }>
         { props.children }
       </KeyboardAvoidingView>
    )   
  }
}

const App = () => {
  SplashScreen.hide();
  console.disableYellowBox = true;
  const [loaded] = useFonts({
    'Montserrat-Regular': require('#/assets/fonts/Montserrat-Regular.ttf'),
    'Montserrat-Medium':    require('#/assets/fonts/Montserrat-Medium.ttf'),
    'Montserrat-Light':     require('#/assets/fonts/Montserrat-Light.ttf'),
    'Montserrat-Bold':      require('#/assets/fonts/Montserrat-Bold.ttf'),
    'Montserrat-ExtraBold': require('#/assets/fonts/Montserrat-ExtraBold.ttf'),
  });
if (!loaded) return null;
  return (
    <View style={ { flex: 1 } }>
      <GeneralStatusBarColor backgroundColor={ Colors.blue } barStyle="light-content" />
      <RootSiblingParent>
        <Provider store={ store }>
           <PersistGate loading={ null } persistor={ persistor }>
             <AvoidingView>
               < AppContainer />
             </AvoidingView>
           </PersistGate>
        </Provider>
      </RootSiblingParent>
    </View>
  )
}

// const App = () => {
//   const devices = useCameraDevices()
//   const device = devices.back
//   if (device == null) {
//     return <ActivityIndicator size={20} color={'red'} />;
//   }
//   return (
//     <Camera
//       style={{flex: 1}}
//       device={device}
//       isActive={true}
//       frameProcessorFps={'auto'}
//     />
//   );
// }

export default App;

