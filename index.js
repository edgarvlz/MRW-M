import { registerRootComponent } from 'expo';
import { LogBox,AppRegistry } from 'react-native';

import App from './App';
import {name as appName} from './app.json';

LogBox.ignoreAllLogs(
    ["exported from 'deprecated-react-native-prop-types'.",
    'Require cycle:',  
    ])
// registerRootComponent calls AppRegistry.registerComponent('main', () => App);
// It also ensures that whether you load the app in Expo Go or in a native build,
// the environment is set up appropriately
AppRegistry.registerComponent(appName, () => App);
